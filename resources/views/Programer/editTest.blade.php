@extends('layouts.MasterProgramer')
@section('content')
@include('sweet::alert')
<div class="page-inner">
    <div class="page-header">
        <h4 class="page-title">Applay Project</h4>
        <ul class="breadcrumbs">
            <li class="nav-home">
                <a href="#">
                    <i class="flaticon-home"></i>
                </a>
            </li>
            <li class="separator">
                <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
                <a href="/FormTest">Task Project</a>
            </li>
            <li class="separator">
                <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
                <a href="/Task/create">Apllay New Task</a>
            </li>
        </ul>
    </div>

 
                                             
                        

        <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Test Yang Akan Dibuat</div>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />@endif
                </div>
                <form method="post" action="{{ route('FormTest.update', $DataTesting[0]->id_testing) }}">
                    @method('PATCH')
                    @csrf
                <div class="card-body">                               
                        <div class="row">
                                <div class="col-md-6 pr-0">
                                    <div class="form-group ">
                                    <label for="recipient-name" class="col-form-label">Nama Test :</label>
                                    <input type="hidden" name="no_qa" value="{{$DataTesting[0]->no_qa}}" readonly class=" form-control">
                                    <!-- <input type="text" name="status2" value="{{$DataTesting[0]->status}}" readonly class=" form-control"> -->
                                    <input type="hidden" name="id_task" value="{{$DataTesting[0]->id_task}}" readonly class=" form-control">
                                    <input type="hidden" name="id_project" value="{{$DataTesting[0]->id_project}}" readonly class=" form-control">
                                    <input type="hidden" name="id_ussers" value="{{$DataTesting[0]->id_ussers}}" readonly class=" form-control">
                                    <input type="hidden" name="nama_project" value="{{$DataTesting[0]->nama_project}}" readonly class=" form-control">
                                    
                                    
                                        <input type="text" name="nama_test" value="{{$DataTesting[0]->nama_task}}" readonly class=" form-control">
                                        @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                        @endif
                                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                    </div>
                                </div>
                            
                               
                                <div class="col-md-6 pr-0">
                                    <div class="form-group ">
                                    <label for="recipient-name" class="col-form-label">Jenis Test :</label>
                                    <input type="text" name="jenis_test" value="{{$DataTesting[0]->jenis_test}}" readonly class=" form-control">

                                        @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('jenis_test') }}</strong>
                                        </span>
                                        @endif
                                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                    </div>
                                </div>


                               
                                <div class="col-md-6">
                                    <div class="form-group ">
                                        <label for="recipient-name" class="col-form-label">Status</label>
                                        <div class="row gutters-xs">
													<div class="col-auto">
														<label class="colorinput">
															<input name="status_testing" type="checkbox" value="OnProgres" <?php if ($DataTesting[0]->status_testing == 'OnProgres' ){echo 'checked';} ?>
                                                            data-toggle="tooltip"  data-original-title="On Progres" class="colorinput-input">
															<span class="colorinput-color bg-primary"></span>
														</label>
													</div>
													<div class="col-auto">
														<label class="colorinput">
															<input name="status_testing" type="checkbox" value="ReadyToTes" <?php if ($DataTesting[0]->status_testing == 'ReadyToTes' ){echo 'checked';} ?>  
                                                            data-toggle="tooltip"  data-original-title="Ready To Tes" class="colorinput-input">
															<span class="colorinput-color bg-secondary"></span>
														</label>
													</div>
													<div class="col-auto">
														<label class="colorinput">
															<input name="status_testing" type="checkbox" value="Testing" <?php if ($DataTesting[0]->status_testing == 'Testing' ){echo 'checked';} ?>  
                                                            data-toggle="tooltip"  data-original-title="Testing" value="info" class="colorinput-input">
															<span class="colorinput-color bg-info"></span>
														</label>
													</div>
													<div class="col-auto">
														<label class="colorinput">
															<input name="status_testing" type="checkbox" value="Issue"  <?php if ($DataTesting[0]->status_testing == 'Issue' ){echo 'checked';} ?>  
                                                            data-toggle="tooltip"  data-original-title="Issue" value="success" class="colorinput-input">
															<span class="colorinput-color bg-success"></span>
														</label>
													</div>
													<div class="col-auto">
														<label class="colorinput">
															<input name="status_testing" type="checkbox" value="Closed"  <?php if ($DataTesting[0]->status_testing == 'Closed' ){echo 'checked';} ?>   
                                                            data-toggle="tooltip"  data-original-title="Closed" class="colorinput-input">
															<span class="colorinput-color bg-danger"></span>
														</label>
													</div>
													<div class="col-auto">
														<label class="colorinput">
															<input name="status_testing" type="checkbox"  value="Solved" <?php if ($DataTesting[0]->status_testing == 'Solved' ){echo 'checked';} ?> 
                                                             data-toggle="tooltip"  data-original-title="Solved" class="colorinput-input">
															<span class="colorinput-color bg-warning"></span>
														</label>
													</div>
												</div>
                                        @if ($errors->has('nama_task'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('nama_task') }}</strong>
                                        </span>
                                        @endif
                                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="form-group ">
                                        <label for="recipient-name" class="col-form-label">Nama Developer</label>
                                        <input type="text" name="nama_developer" value="{{$DataTesting[0]->name}}" readonly class=" form-control">
                                        @if ($errors->has('nama_task'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('nama_task') }}</strong>
                                        </span>
                                        @endif
                                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                    </div>
                                </div>

                               
                               
                            </div>
                           
                        </div>              
               </div>
            </div>
        </div>
        <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Test Yang Akan Dibuat</div>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />@endif
                </div>
                <div class="col-md-12 pr-0">
                                    <div class="form-group ">
                                    <label for="recipient-name" class="col-form-label">Keterangan :</label>
                                    {!! html_entity_decode($DataTesting[0]->keterangan_test) !!}
                                    @if ($errors->has('role'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('role') }}</strong>
                                    </span>
                                    @endif
                                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary btn-lg btn-block">Simpan</button>
            </div>
            
        </div>
        </div>
    </form>
                 

    @endsection
