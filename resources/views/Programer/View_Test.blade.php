@extends('layouts.MasterProgramer')
@section('content')
@include('sweet::alert')
<div class="row mt--2">
    <div class="col-md-12">
        <div class="card full-height">
            <div class="card-body">
            @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div><br />
            @endif
                <div class="card-title">Pilih Task yang akan di tampilkan</div>
                <div class="card-category"></div>
                <select class="form-control" name="id_project" id="PilidData">
                    <option value="">Pilih Data Task</option>
                    @foreach($DataTest as $DataTest )
                    <option value="{{ $DataTest->id_testing }}">{{ $DataTest->nama_project }} - {{ $DataTest->nama_test }} - {{ $DataTest->jenis_test }} </option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    </div>
 
    <div class="row mt--6">
        <div class="col-md-12">
            <div id="TampilData"> </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#PilidData').on('change',function () {
                
                var id = $(this).val();
                $.ajax({
                    type: 'GET',
                    url: "/PilihTesting/"+id,
                    success: function (respons) {
                        console.log(respons);
                        $('#TampilData').html("");

                        $.each(respons,function (index,element) {
                            var status_testing = '';
                                if (element.status_testing == 'OnProgres') {
                                    cetakstatus = '<span class="colorinput-color bg-primary"  data-toggle="tooltip"  data-original-title="On Progres" class="colorinput-input"> <center style=" margin-top: 5px;"> O </center>';
                                }else if (element.status_testing == 'Testing') {
                                    cetakstatus = '<span class="colorinput-color bg-info"  data-toggle="tooltip"  data-original-title="Testing" class="colorinput-input"><center style=" margin-top: 5px;"> T </center>';
                                }else  if (element.status_testing == 'ReadyToTes') {
                                    cetakstatus = '<span class="colorinput-color bg-secondary"  data-toggle="tooltip"  data-original-title="Ready To Tes" class="colorinput-input"><center style=" margin-top: 5px;"> R </center>';
                                }else  if (element.status_testing == 'Issue') {
                                    cetakstatus = '<span class="colorinput-color bg-success"  data-toggle="tooltip"  data-original-title="Issue" class="colorinput-input"><center style=" margin-top: 5px;"> I </center>';
                                }else  if (element.status_testing == 'Solved') {
                                    cetakstatus = '<span class="colorinput-color bg-warning" data-toggle="tooltip"  data-original-title="Sloved" class="colorinput-input"><center style=" margin-top: 5px;"> S </center>';
                                }else  if (element.status_testing == 'Closed') {
                                    cetakstatus = '<span class="colorinput-color bg-danger" data-toggle="tooltip"  data-original-title="Closed" class="colorinput-input"><center style=" margin-top: 5px;"> C </center></span>';
                                }
                            $('#TampilData').append(`
                            
                            
                                    <div class="card full-height">
                                        <div class="card-body">
                                        <div class="card-header">
                                                <div class="d-flex align-items-center">
                                                    <h4 class="card-title">`+element.nama_task+`</h4>
                                                    <button class="btn btn-primary btn-round ml-auto"
                                                        onclick="window.location.href = '/FormTest/`+element.id_testing+`/edit';">
                                                        <i class="fa fa-check"></i>

                                                    </button>
                                                </div>
                                            </div>
                                            <div class="card-category"></div>
                                            <div class="d-flex flex-wrap justify-content-around pb-2 pt-2">
                                                <table class="display table table-striped table-hover">
                                                    <tr>
                                                        <td>Kode Testing</td>
                                                        <td>Nama Tes</td>
                                                        <td>Jenis Tes</td>
                                                        <td colspan='2'><center>Status</center></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>TST-0`+element.id_testing+`</td>
                                                        <td>`+element.nama_test+`</td>
                                                        <td>`+element.jenis_test+`</td>
                                                        <td colspan='2'>`+element.status_testing+`</td>
                                                        <td> `+cetakstatus+`</td>
                                                       
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    
                             

                            `);
                        });
                    },
                    error: function (e,log) {
                        // DO SOMETHING WHEN ERROR
                    }
                });                
                
            })
        })
    </script>
@endsection