@extends('layouts.file')
@section('login')
<div class="login-box">
  <div class="login-logo">
    <a href="dashboard"><b>Monitoring </b>Proyek</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Lost Password</p>

    <form action="login" method="post">
      <div class="form-group has-feedback">
        <input type="email"  required="required" class="form-control" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="row">
      <div class="col-xs-12">
          <a href="login" type="submit" onclick="return confirm('Suksses Di Kirim Ke Email?')" class="btn btn-block btn-primary btn-flat"> Lost Password</a>
          <input type="submit" name="login"  onclick="return confirm('Suksses Di Kirim Ke Email?')" class="btn btn-block btn-primary btn-flat" value="Lost Password">
      </div>
     
        </div>
        <!-- /.col -->
        <!-- /.col -->
      </div>
    </form>

  </div>
@endsection