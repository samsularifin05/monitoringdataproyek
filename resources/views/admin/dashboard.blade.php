@extends('layouts.MasterAdmin')
@section('content')
@include('sweet::alert')
 <!-- Main content -->
 
 <?php
  date_default_timezone_set("Asia/Jakarta");
  $b = time();
  $hour = date("G",$b);
  if ($hour>=0 && $hour<=11){
	  $selamat = "Selamat Pagi ";
  }elseif ($hour >=12 && $hour<=14){
	  $selamat = "Selamat Siang  ";
  }elseif ($hour >=15 && $hour<=17){
	  $selamat = "Selamat Sore  ";
  }elseif ($hour >=17 && $hour<=18){
	  $selamat = "Selamat Petang ";
  }elseif ($hour >=19 && $hour<=23){
	  $selamat = "Selamat Malam ";
  }

?>
 <div class="panel-header bg-primary-gradient">
					<div class="page-inner py-5">
						<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
							<div>
								<h2 class="text-white pb-2 fw-bold">Dashboard</h2>
								
								<h5 class="text-white op-7 mb-2"><?php echo $selamat ?> {{Auth::user()->name}}</h5>
							</div>
							<div class="ml-md-auto py-2 py-md-0">
						
								<a href="#" class="btn btn-white btn-border btn-round mr-2"><span id="output" ></span></a>
								<a href="#" class="btn btn-white btn-border btn-round mr-2"><?php echo date('D-M-Y')?></a>
							
							</div>
						</div>
					</div>
				</div>

				<div class="page-inner mt--5">
					<div class="row mt--2">
						<div class="col-md-6">
							<div class="card full-height">
								<div class="card-body">
									<div class="card-title">Target Progres Task</div>
									<div class="card-category">
									<div class="card-title"> </div>
									<div class="card-category"></div>
									<select class="form-control" name="id_project" id="PilidData">
										<option value="">Pilih Data Project</option>
										@foreach($DataProject as $DataProject )
										<option value="{{ $DataProject->id_project }}">{{ $DataProject->nama_project }}</option>
										@endforeach
									</select>
									</div>
									<div class="d-flex flex-wrap justify-content-around pb-2 pt-4" id="TampilData">
									<center>
									Data Masih Kosong </center>
									
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="card full-height">
								<div class="card-body">
									<div class="card-title">Progres Project</div>
									<div class="row py-4">
										<div class="col-md-4 d-flex flex-column justify-content-around" id="TampilData2">
											
										
										</div>
										<div class="col-md-8">
											<div id="chart-container">
												<canvas id="multipleLineChart"></canvas>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<!-- <div class="row mt--2">
						<div class="col-md-6">
							<div class="card full-height">
								<div class="card-body">
									<div class="card-title">Target Progres Task</div>
									<div class="card-category">
									<div class="card-title"> </div>
									<div class="card-category" id="TampilData3"></div>
								
									</div>
									<div class="d-flex flex-wrap justify-content-around pb-2 pt-4" id="TampilData">
									
									
									</div>
								</div>
							</div>
						</div> -->

					<div class="row">
						<div class="col-md-4">
							<div class="card">
								<div class="card-header">
									<div class="card-title">Data Project</div>
								</div>
								@foreach ($ViewProject as $Data)
								
								<div class="card-body pb-0">
									<div class="d-flex">
										<div class="avatar avatar-img rounded-circle">
										<span class="avatar-title rounded-circle border border-white bg-info"> <?php echo  substr($Data->nama_project , 0, 1) ?> </span>
										</div>
										<div class="flex-1 pt-1 ml-2">
											<h6 class="fw-bold mb-1"></h6>
											<small class="text-muted"><?php echo $Data->nama_project ?></small><br>
											<?php echo $Data->perusahaan ?>
										</div>
										<div class="d-flex ml-auto align-items-center">
											<h3 class="text-info fw-bold"><?php echo $Data->tanggal_mulai ?></h3>
										</div>
									</div>
									<div class="separator-dashed"></div>
							
								</div>
								@endforeach
							</div>
						</div>
						<div class="col-md-4">
							<div class="card">
								<div class="card-header">
									<div class="card-title">Data Member Banyak Contribusi</div>
								</div>
								@foreach ($Member as $Data)
								
								<div class="card-body pb-0">
									<div class="d-flex">
										<div class="avatar avatar-img rounded-circle">
										<span class="avatar-title rounded-circle border border-white bg-info"> <?php echo  substr($Data->name , 0, 1) ?> </span>
										</div>
										<div class="flex-1 pt-1 ml-2">
											<h6 class="fw-bold mb-1"></h6>
											<small class="text-muted"><?php echo $Data->name ?></small><br>
											<?php echo $Data->role ?>
										</div>
										<div class="d-flex ml-auto align-items-center">
											<h3 class="text-info fw-bold"><?php echo $Data->no_tlp ?></h3>
										</div>
									</div>
									<div class="separator-dashed"></div>
							
								</div>
								@endforeach
							</div>
						</div>
						
						<div class="col-md-4">
							<div class="card card-primary bg-primary-gradient">
								<div class="card-body">
									<h4 class="mt-3 b-b1 pb-2 mb-4 fw-bold">Kirim Pesan</h4>
									<form method="post" action="{{ route('PM.store') }}">
										@csrf
									<h4>Pilih Member</h4>
									<select class="form-control"  name="id_users" id="PiliData">
                                        <option value="">Pilih Nama QA</option>
													@foreach($DataMember as $DataMember )
													<option value="{{ $DataMember->id_users }}" >{{ $DataMember->name }}</option>
													@endforeach
										</select>
									<h4>No Tlp</h4>
									<input type="text" name="nope" class="form-control">
									<h4>Pesan</h4>
									<textarea class="form-control" name="pesan">
									</textarea><br>
									
								<button type="submit" class="btn btn-primary btn-block">Kirim</button>
								</form>
								</div>
							</div>
						</div>
					</div>
					<!-- <div class="row">
						<div class="col-md-6">
							<div class="card full-height">
								<div class="card-header">
									<div class="card-title">Feed Activity</div>
								</div>
								<div class="card-body">
									<ol class="activity-feed">
										<li class="feed-item feed-item-secondary">
											<time class="date" datetime="9-25">Sep 25</time>
											<span class="text">Responded to need <a href="#">"Volunteer opportunity"</a></span>
										</li>
										<li class="feed-item feed-item-success">
											<time class="date" datetime="9-24">Sep 24</time>
											<span class="text">Added an interest <a href="#">"Volunteer Activities"</a></span>
										</li>
										<li class="feed-item feed-item-info">
											<time class="date" datetime="9-23">Sep 23</time>
											<span class="text">Joined the group <a href="single-group.php">"Boardsmanship Forum"</a></span>
										</li>
										<li class="feed-item feed-item-warning">
											<time class="date" datetime="9-21">Sep 21</time>
											<span class="text">Responded to need <a href="#">"In-Kind Opportunity"</a></span>
										</li>
										<li class="feed-item feed-item-danger">
											<time class="date" datetime="9-18">Sep 18</time>
											<span class="text">Created need <a href="#">"Volunteer Opportunity"</a></span>
										</li>
										<li class="feed-item">
											<time class="date" datetime="9-17">Sep 17</time>
											<span class="text">Attending the event <a href="single-event.php">"Some New Event"</a></span>
										</li>
									</ol>
								</div>
							</div>
						</div> -->
					
						<div class="col-md-12">
							<div class="card full-height">
								<div class="card-header">
									<div class="card-head-row">
										<div class="card-title">Detail Aktifitas</div>
										<div class="card-tools">
											
										</div>
									</div>
								</div>
								<div class="card-body">
								@if($logs->count())
								@foreach($logs as $key => $log)
									<div class="d-flex">
										<div class="avatar avatar-online">
											<span class="avatar-title rounded-circle border border-white bg-info"><?php echo substr($log->nama_users, 0, 1)?></span>
										</div>
										<div class="flex-1 ml-3 pt-1">
											<h6 class="text-uppercase fw-bold mb-1">{{ $log->nama_users }}<span class="text-warning pl-3"><?php echo substr($log->created_at, 0,10)?></span></h6>
											<span class="text-muted">{{ $log->subject }}</span>
										</div>
										<div class="float-right pt-1">
											<small class="text-muted"><?php echo substr($log->created_at, 10,20)?></small>
										</div>
									</div>
									@endforeach
									@endif
								</div>
							</div>
						</div>
					</div>
					

<script type="text/javascript">
// 1 detik = 1000
window.setTimeout("waktu()",1000);
function waktu() {
var tanggal = new Date();
setTimeout("waktu()",1000);
document.getElementById("output").innerHTML = tanggal.getHours()+":"+tanggal.getMinutes()+":"+tanggal.getSeconds();
}

</script>

<script>
        $(document).ready(function () {
            $('#PilidData').on('change',function () {
                
                var id = $(this).val();
                $.ajax({
                    type: 'GET',
                    url: "/PilihDataProject/"+id,
                    success: function (respons) {
                        console.log(respons);
						
                        $('#TampilData').html("");
						$('#TampilData3').html("");
						
							if (typeof respons.Testing === 'undefined') {
								Testing = '0';
							}else{
								Testing = respons.Testing;
							}
							
							if (typeof respons.OnProgres === 'undefined') {
								OnProgres = '0';
							}else{
								OnProgres = respons.OnProgres;
							}

							if (typeof respons.ReadyToTes === 'undefined') {
								ReadyToTes = '0';
							}else{
								ReadyToTes = respons.ReadyToTes;
							}
							
							if (typeof respons.Issue === 'undefined') {
								Issue = '0';
							}else{
								Issue = respons.Issue;
							}
							if (typeof respons.Closed === 'undefined') {
								Closed = '0';
							}else{
								Closed = respons.Closed;
							}
							if (typeof respons.Solved === 'undefined') {
								Solved = '0';
							}else{
								Solved = respons.Solved;
							}

						var hasil =parseInt(Testing) + parseInt(OnProgres)+ parseInt(Issue) + parseInt(Closed) + parseInt(Solved);
						var data = '';
						

						$('#TampilData3').append(`
						
								<table class="display table table-striped table-hover">
                                                    <tr>
                                                        <td>Aman</td>
                                                        <td>Warning</td>
                                                        <td>Debig</td>
                                                    </tr>
                                                    <tr>
                                                        <td>`+data+`</td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                </table>

						`);


                        $('#TampilData').append(`
							<div class="px-2 pb-2 pb-md-0 text-center">
											<div id="OnProgres"></div>
											<h6 class="fw-bold mt-3 mb-0">OnProgres</h6>
										
										</div>
										<div class="px-2 pb-2 pb-md-0 text-center">
											<div id="ReadyToTes"></div>
											<h6 class="fw-bold mt-3 mb-0">Ready To Test</h6>
								
										</div>
										<div class="px-2 pb-2 pb-md-0 text-center">
											<div id="Testing"></div>
											<h6 class="fw-bold mt-3 mb-0">Testing</h6>
									
										</div>
										<div class="px-2 pb-2 pb-md-0 text-center">
											<div id="Issue"></div>
											<h6 class="fw-bold mt-3 mb-0">Issue</h6>
											
										</div>
									
										<div class="px-2 pb-2 pb-md-0 text-center">
											<div id="Solved"></div>
											<h6 class="fw-bold mt-3 mb-0">Solved</h6>
											
										</div>

                            `);

						$('#TampilData2').html("");
						Circles.create({
							id:'OnProgres',
							radius:30,
							value: OnProgres,
							maxValue:100,
							width:7,
							text: OnProgres,
							colors:['#f1f1f1', '#48ABF7'],
							duration:400,
							wrpClass:'circles-wrp',
							textClass:'circles-text',
							styleWrapper:true,
							styleText:true
						})
						Circles.create({
							id:'ReadyToTes',
							radius:30,
							value:ReadyToTes,
							maxValue:100,
							width:7,
							text: ReadyToTes,
							colors:['#f1f1f1', '#5B5AC1'],
							duration:400,
							wrpClass:'circles-wrp',
							textClass:'circles-text',
							styleWrapper:true,
							styleText:true
						})
						Circles.create({
							id:'Testing',
							radius:30,
							value:Testing,
							maxValue:100,
							width:7,
							text: Testing,
							colors:['#f1f1f1', '#48ABF7'],
							duration:400,
							wrpClass:'circles-wrp',
							textClass:'circles-text',
							styleWrapper:true,
							styleText:true
						})
							
						Circles.create({
							id:'Issue',
							radius:30,
							value:Issue,
							maxValue:100,
							width:7,
							text: Issue,
							colors:['#f1f1f1', '#31CE36'],
							duration:400,
							wrpClass:'circles-wrp',
							textClass:'circles-text',
							styleWrapper:true,
							styleText:true
						})
						Circles.create({
							id:'Solved',
							radius:30,
							value:Solved,
							maxValue:100,
							width:7,
							text: Solved,
							colors:['#f1f1f1', '#FFAD46'],
							duration:400,
							wrpClass:'circles-wrp',
							textClass:'circles-text',
							styleWrapper:true,
							styleText:true
						})



						var multipleLineChart = document.getElementById('multipleLineChart').getContext('2d');

							var myMultipleLineChart = new Chart(multipleLineChart, {
								type: 'line',
								data: {
									labels: ["Progres Project"],
									datasets: [{
										label: "Project",
										borderColor: "#1d7af3",
										pointBorderColor: "#FFF",
										pointBackgroundColor: "#1d7af3",
										pointBorderWidth: 2,
										pointHoverRadius: 4,
										pointHoverBorderWidth: 1,
										pointRadius: 4,
										backgroundColor: 'transparent',
										fill: true,
										borderWidth: 2,
										data: [Closed]
									},]
								},
								options : {
									responsive: true,
									maintainAspectRatio: false,
									legend: {
										position: 'top',
									},
									tooltips: {
										bodySpacing: 4,
										mode:"nearest",
										intersect: 0,
										position:"nearest",
										xPadding:10,
										yPadding:10,
										caretPadding:10
									},
									layout:{
										padding:{left:15,right:15,top:15,bottom:15}
									}
								}
							});
					
						$('#TampilData2').append(`
						
											<div>
												<h6 class="fw-bold text-uppercase text-success op-8">Progres Projeck</h6>
												<h3	 class="fw-bold">`+Closed+`%</h3>
											</div>
											<div>
												<h6 class="fw-bold text-uppercase text-danger op-8">Total Task</h6>
												<h3 class="fw-bold">`+hasil+`</h3>
											</div>

						`);

						
                        
                    },
                    error: function (e,log) {
                        // DO SOMETHING WHEN ERROR
                    }
                });                
                
            })
        })

		
						



		
    </script>

    @endsection
	