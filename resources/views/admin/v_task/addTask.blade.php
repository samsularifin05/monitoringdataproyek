@extends('layouts.MasterAdmin')
@section('content')
<div class="page-inner">
    <div class="page-header">
        <h4 class="page-title">Add New Task</h4>
        <ul class="breadcrumbs">
            <li class="nav-home">
                <a href="#">
                    <i class="flaticon-home"></i>
                </a>
            </li>
            <li class="separator">
                <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
                <a href="/Task">Form Task</a>
            </li>
            <li class="separator">
                <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
                <a href="/Task/create">Add New Task</a>
            </li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Informasi Task Yang akan di buat</div>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />@endif
                </div>
                <div class="card-body">
                    <form method="post" action="{{ route('Task.store') }}">
                        {{ csrf_field() }}
                        <div class="col-md-12">
                        <div id="TampilData"> </div>
                        <div id="TampilData2"> </div>
                    </div>
                        <div class="row">
                                <div class="col-md-6 pr-0">
                                    <div class="form-group ">
                                        <label for="recipient-name" class="col-form-label">Pilih Project :</label>
                                        <select class="form-control"  name="id_project" id="PiliData2" >
                                                    <option value="">Pilih Data Project</option>
													@foreach($DataProject as $DataProject )
													<option value="{{ $DataProject->id_project }}" >{{ $DataProject->nama_project }}</option>
													@endforeach
												</select>
                                        @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                        @endif
                                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                    </div>
                                </div>
                            
                                
                                <div class="col-md-6 pr-0">
                                    <div class="form-group ">
                                    <label for="recipient-name" class="col-form-label">Pilih QA :</label>
                                        <select class="form-control"  name="id_users" id="PiliData">
                                        <option value="">Pilih Nama QA</option>
													@foreach($DataMember as $DataMember )
													<option value="{{ $DataMember->id_users }}" >{{ $DataMember->name }}</option>
													@endforeach
												</select>
                                        @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                        @endif
                                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group ">
                                        <label for="recipient-name" class="col-form-label">Nama Task :</label>
                                        <input id="nama_task" type="text" placeholder="Nama Task"
                                            class="form-control{{ $errors->has('nama_task') ? ' is-invalid' : '' }}"
                                            name="nama_task" value="{{ old('nama_task') }}" required>
                                        @if ($errors->has('nama_task'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('nama_task') }}</strong>
                                        </span>
                                        @endif
                                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                    </div>
                                </div>

                                <div class="col-md-6 pr-0">
                                    <div class="form-group ">
                                    
                                        <label for="recipient-name" class="col-form-label">Status :</label>
                                        <div class="row gutters-xs">
													<div class="col-auto">
														<label class="colorinput">
															<input name="status" type="checkbox" value="OnProgres"  data-toggle="tooltip"  data-original-title="On Progres" class="colorinput-input">
															<span class="colorinput-color bg-primary"></span>
														</label>
													</div>
													<div class="col-auto">
														<label class="colorinput">
															<input name="status" type="checkbox"  value="ReadyToTes"  data-toggle="tooltip"  data-original-title="Ready To Tes" class="colorinput-input">
															<span class="colorinput-color bg-secondary"></span>
														</label>
													</div>
													<div class="col-auto">
														<label class="colorinput">
															<input name="status" type="checkbox" value="Testing"  data-toggle="tooltip"  data-original-title="Testing" value="info" class="colorinput-input">
															<span class="colorinput-color bg-info"></span>
														</label>
													</div>
													<div class="col-auto">
														<label class="colorinput">
															<input name="status" type="checkbox" value="Issue"  data-toggle="tooltip"  data-original-title="Issue" value="success" class="colorinput-input">
															<span class="colorinput-color bg-success"></span>
														</label>
													</div>
													<div class="col-auto">
														<label class="colorinput">
															<input name="status" type="checkbox" value="Closed"   value="Closed"  data-toggle="tooltip"  data-original-title="Closed" class="colorinput-input">
															<span class="colorinput-color bg-danger"></span>
														</label>
													</div>
													<div class="col-auto">
														<label class="colorinput">
															<input name="status" type="checkbox" value="Solved" value="Solved"  data-toggle="tooltip"  data-original-title="Sloved" class="colorinput-input">
															<span class="colorinput-color bg-warning"></span>
														</label>
													</div>
												</div>
                                            <!-- <select class="form-control"  name="status" >
                                            
                                            <option value="">Pilih Status</option>                                            
                                                    <option value="OnProgres"> <span class="colorinput-color bg-primary"></span> On Progres</option>
                                                    <option  value="ReadyToTes"><div  class="colorinput-color bg-warning" ></div>Ready To Tes</option>
                                                    <option  value="Testing" >Testing</option>
                                                    <option  value="Issue" >Issue</option>
                                                    <option  value="Solved" >Solved</option>
                                                    <option  value="Closed" >Closed</option> 
										</select> -->
       
                                        @if ($errors->has('status'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('status') }}</strong>
                                        </span>
                                        @endif
                                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="col-sm-14">
                                <div class="form-group ">
                                    <label for="recipient-name" class="col-form-label">Keterangan :</label>
                                    <textarea class="form-control" name="keterangan" placeholder="Keterangan"></textarea>
                                    @if ($errors->has('role'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('role') }}</strong>
                                    </span>
                                    @endif
                                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                </div>
                                <div class="modal-footer no-bd">
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#PiliData2').on('change',function () {
                var id = $(this).val();
                $.ajax({
                    type: 'GET',
                    url: "/ProjectDetail/"+id,
                    success: function (respons) {
                        console.log(respons);
                        $('#TampilData2').html("");

                        $.each(respons,function (index,element) {
                            $('#TampilData2').append(`
                            <input type=hidden name='nama_project' value='`+element.nama_project+`'>
                            `);
                        });
                    },
                    error: function (e,log) {
                        // DO SOMETHING WHEN ERROR
                    }
                });                
                
            })
        })
    </script>
    <script>
        $(document).ready(function () {
            $('#PiliData').on('change',function () {
                var id = $(this).val();
                $.ajax({
                    type: 'GET',
                    url: "/PilihQa/"+id,
                    success: function (respons) {
                        console.log(respons);
                        $('#TampilData').html("");

                        $.each(respons,function (index,element) {
                            $('#TampilData').append(`
                            <input type=hidden name='nope' value='`+element.no_tlp+`'>
                            <input type=hidden name='nama_qa' value='`+element.name+`'>
                            `);
                        });
                    },
                    error: function (e,log) {
                        // DO SOMETHING WHEN ERROR
                    }
                });                
                
            })
        })
    </script>

    @endsection