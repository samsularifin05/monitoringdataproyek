@extends('layouts.MasterAdmin')
@section('content')
@include('sweet::alert')

<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-center">
                <h4 class="card-title">Form Task</h4>
                <button class="btn btn-primary btn-round ml-auto"
                    onclick="window.location.href = '{{route('Task.create')}}';">
                    <i class="fa fa-plus"></i>

                </button>
            </div>
        </div>
        <div class="card-body">
            @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}

            </div><br />
            @endif
            
            <div class="table-responsive">
                <table id="add-row" class="display table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>No </th>
                            <th>Nama Task</th>
                            <th>Nama Project</th>
                            <th><center>Status</center></th>
                            <!-- <th></th> -->
                            <!-- <th>keterangan</th> -->
                            <th><center>Aksi</center></th>
                            
                        </tr>
                    </thead>
                   
                    <tbody>
               
                 
                        <?php $no = 0;?>
                        @foreach($Data as $DataTask )
                       
                        <?php 
                            $warna = "";
                            if($DataTask->status== "OnProgres"){
                                $warna = '<span class="colorinput-color bg-primary"  data-toggle="tooltip"  data-original-title="On Progres" class="colorinput-input"> 
                                <center style=" margin-top: 5px;"> O </center>';
                            }elseif($DataTask->status== "Testing"){
                                $warna = '<span class="colorinput-color bg-info"  data-toggle="tooltip"  data-original-title="Testing" class="colorinput-input"> 
                                <center style=" margin-top: 5px;"> T </center>';
                            }elseif($DataTask->status== "ReadyToTes"){
                                $warna = '<span class="colorinput-color bg-secondary"  data-toggle="tooltip"  data-original-title="Ready To Tes" class="colorinput-input"> 
                                <center style=" margin-top: 5px;"> R </center>';
                            }elseif($DataTask->status== "Issue"){
                                $warna = '<span class="colorinput-color bg-success"  data-toggle="tooltip"  data-original-title="Issue" class="colorinput-input"> 
                                <center style=" margin-top: 5px;"> I </center>';
                            }elseif($DataTask->status== "Sloved"){
                                $warna = '<span class="colorinput-color bg-warning" data-toggle="tooltip"  data-original-title="Sloved" class="colorinput-input"> 
                                <center style=" margin-top: 5px;"> S </center>';
                            }elseif($DataTask->status== "Closed"){
                                $warna = '<span class="colorinput-color bg-danger" data-toggle="tooltip"  data-original-title="Closed" class="colorinput-input"> 
                                <center style=" margin-top: 5px;"> C </center></span>';
                            }

                        ?>
                      
                        <?php $no++ ;?>
                        <tr>
                        
                            <td>{{$no }}</td>
                            <td>{{$DataTask->nama_task}}</td>
                            <td>{{$DataTask->nama_project}}</td>
                            <!-- <td >{{$DataTask->status}}</td> -->
                            <td align="center"> 
                            <a href="#" data-id="{{ $DataTask->id_task }}"
                             data-toggle="modal" data-target="#statusModal" onclick="showStatus(this)">
                            <?php echo $warna ?> </a></td>
                            <!-- <td>{{$DataTask->keterangan}}</td> -->
                            <td align="center">
                                <div class="form-button-action">
                                    <button type="button" data-toggle="tooltip" class="btn btn-link btn-primary"
                                        data-original-title="Edit Project"
                                        onclick="window.location.href = '/Task/{{$DataTask->id_task}}/edit';"><i
                                            class="fa fa-edit"></i>
                                    </button>
                                    <form action="{{ route('Task.destroy', $DataTask->id_task)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button data-toggle="tooltip" title="" class="btn btn-link btn-danger"
                                            data-original-title="Remove" type="submit">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </form>
                                </div>

                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="statusModal" tabindex="-1" role="dialog" aria-labelledby="statusModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="statusModalLabel">Edit Status</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form method="post" action="/ubahStatus">
         
          @csrf

              <input type="text" id="id_task" name="id_task" >
              <input type="text" id="nama_task" name="nama_task" >
              <input type="text" id="nope" name="nope" >
              <input type="text" id="nama_qa" name="nama_qa" >
              <input type="text" id="nama_project" name="nama_project" >
           
           
              <div class="row gutters-xs">
                                        <div class="col-auto">
                                            <label class="colorinput">
                                                <input name="status" type="checkbox" value="OnProgres" id="OnProgres"
                                                    data-toggle="tooltip" data-original-title="On Progres"
                                                    class="colorinput-input">
                                                <span class="colorinput-color bg-primary"></span>
                                            </label>
                                        </div>
                                        <div class="col-auto">
                                            <label class="colorinput">
                                                <input name="status" type="checkbox" value="ReadyToTes" id="ReadyToTes"
                                                  
                                                    data-toggle="tooltip" data-original-title="Ready To Tes"
                                                    class="colorinput-input">
                                                <span class="colorinput-color bg-secondary"></span>
                                            </label>
                                        </div>
                                        <div class="col-auto">
                                            <label class="colorinput">
                                                <input name="status" type="checkbox" value="Testing"  id="Testing"
                                               
                                                    data-toggle="tooltip" data-original-title="Testing" value="info"
                                                    class="colorinput-input">
                                                <span class="colorinput-color bg-info"></span>
                                            </label>
                                        </div>
                                        <div class="col-auto">
                                            <label class="colorinput">
                                                <input name="status" type="checkbox" value="Issue" id="Issue"
                                                    data-toggle="tooltip" data-original-title="Issue" value="success"
                                                    class="colorinput-input">
                                                <span class="colorinput-color bg-success"></span>
                                            </label>
                                        </div>
                                        <div class="col-auto">
                                            <label class="colorinput">
                                                <input name="status" type="checkbox" value="Closed" id="Closed"
                                                  
                                                    data-toggle="tooltip" data-original-title="Closed"
                                                    class="colorinput-input">
                                                <span class="colorinput-color bg-danger"></span>
                                            </label>
                                        </div>
                                        <div class="col-auto">
                                            <label class="colorinput">
                                                <input name="status" type="checkbox" value="Sloved" id="Sloved"
                                                    data-toggle="tooltip" data-original-title="Sloved"
                                                    class="colorinput-input">
                                                <span class="colorinput-color bg-warning"></span>
                                            </label>
                                        </div>
                                    </div>

  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>
<script>
//  $(document).ready(function(){
//     // $(document).on('click','.openModal',function(){
//     //     var id = $(this).data('id');
//     //     $('.modal_hiddenid').val(id);
//     //     $('.modal_status_inp').val($(this).data('status-text'))
//     //     var qr_winner_name = $('#qrd_'+id+' .qr_winner_name').html();
//     //     $('.qr_winner_name_show').html(qr_winner_name);
//     // });
//   })

  function showStatus(id) {
      var id_t = $(id).attr('data-id');
    //   console.log(id_task);
    $.ajax({
        type :'GET',
        url : 'Task/getDetailJson/'+id_t,
        success: function (respons) {
            var jsn = $.parseJSON(respons);
            var sts = jsn.status;
            var nama_task = jsn.nama_task;
            var nope = jsn.no_tlp;
            var nama_qa = jsn.name;
            var nama_project = jsn.nama_project;
            
            $('#id_task').val(jsn.id_task).hide();
            $('#nama_task').val(nama_task).hide();
            $('#nope').val(nope).hide();
            $('#nama_qa').val(nama_qa).hide();
            $('#nama_project').val(nama_project).hide();

            if (sts == 'OnProgres'){
                $('#OnProgres').prop('checked', 'true');
            }else if (sts == 'ReadyToTes'){
                $('#ReadyToTes  ').prop('checked', 'true');
            }else if (sts == 'Testing'){
                $('#Testing  ').prop('checked', 'true');
            }else if (sts == 'Issue'){
                $('#Issue  ').prop('checked', 'true');
            }else if (sts == 'Closed'){
                $('#Closed  ').prop('checked', 'true');
            }else if (sts == 'Sloved'){
                $('#Sloved').prop('checked', 'true');
            }else{
                $('#OnProgres').prop('checked', false);
            }
        }
    })
  }
</script>

@endsection