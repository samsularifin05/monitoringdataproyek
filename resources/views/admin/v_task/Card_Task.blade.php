@extends('layouts.MasterAdmin')
@section('content')
<!-- <div class="col-md-3">

@foreach($Data as $DataTask )

    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-center">
                <h4 class="card-title">{{$DataTask->nama_task}}</h4>
            </div>
        </div>
        <div class="card-body">
           
           
        </div>
    </div>
@endforeach
</div> -->


<div class="row mt--2">
@foreach($Data as $DataTask )
						<div class="col-md-6">
							<div class="card full-height">
								<div class="card-body">
									<div class="card-title">{{$DataTask->nama_task}}</div>
									<div class="card-category">{{$DataTask->keterangan}}</div>
									<div class="d-flex flex-wrap justify-content-around pb-2 pt-4">
										<div class="px-2 pb-2 pb-md-0 text-center">
											<div id="circles-1"></div>
											<h6 class="fw-bold mt-3 mb-0">New Users</h6>
										</div>
										<div class="px-2 pb-2 pb-md-0 text-center">
											<div id="circles-2"></div>
											<h6 class="fw-bold mt-3 mb-0">Sales</h6>
										</div>
										<div class="px-2 pb-2 pb-md-0 text-center">
											<div id="circles-3"></div>
											<h6 class="fw-bold mt-3 mb-0">Subscribers</h6>
										</div>
									</div>
								</div>
							</div>
						</div>
    @endforeach					
    </div>

@endsection