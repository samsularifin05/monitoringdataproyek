@extends('layouts.MasterAdmin')
@section('content')
<div class="page-inner">
    <div class="page-header">
        <h4 class="page-title">Edit Task</h4>
        <ul class="breadcrumbs">
            <li class="nav-home">
                <a href="#">
                    <i class="flaticon-home"></i>
                </a>
            </li>
            <li class="separator">
                <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
                <a href="/Task">Form Task</a>
            </li>
            <li class="separator">
                <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
                <a href="/Task/create">Edit New Task</a>
            </li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Informasi Task Yang akan di Edit</div>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />@endif
                </div>
                <div class="card-body">
              
                <form method="post" action="{{ route('Task.update', $Data->id_task) }}">
                    @method('PATCH')
                    @csrf

 
                                             
                        <div class="row">
                                <div class="col-md-6 pr-0">
                                    <div class="form-group ">
                                    <label for="recipient-name" class="col-form-label">Pilih Project :</label>
                                        <select class="js-example-disabled-results form-control select2 " name="id_project">
                                        @foreach($DataProject as $D )
                                           <option value="{{$D->id_project}}" {{($Data->id_project === $D->id_project) ? 'selected' : ''}}
                                           > {{$D->nama_project}} </option>
                                        @endforeach 
										</select>

                                        @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                        @endif
                                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                    </div>
                                </div>
                            
                               
                                <div class="col-md-6 pr-0">
                                    <div class="form-group ">
                                    <label for="recipient-name" class="col-form-label">Pilih QA :</label>
                                    <select class="js-example-disabled-results form-control select2 " name="id_users">
                                        @foreach($DataMember as $M )
                                           <option value="{{$M->id_users}}" {{($Data->id_users === $M->id_users) ? 'selected' : ''}}
                                           > {{$M->name}} </option>
                                        @endforeach 
										</select>
                                        <?php
                                            foreach ($DataMember as $Data1){
                                                if($Data1->id_users == $Data->id_users){
                                                echo "<input type='hidden' value='$Data1->name' name='nama_qa'>";
                                                echo "<input type='hidden' value='$Data1->no_tlp' name='no_qa'>";
                                                }
                                              }

                                            ?>
                                        @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                        @endif
                                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group ">
                                        <label for="recipient-name" class="col-form-label">Nama Task :</label>
                                        <input id="nama_task" type="text" placeholder="Nama Task"
                                            class="form-control{{ $errors->has('nama_task') ? ' is-invalid' : '' }}"
                                            name="nama_task" value="{{$Data->nama_task}}" required>
                                        @if ($errors->has('nama_task'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('nama_task') }}</strong>
                                        </span>
                                        @endif
                                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                    </div>
                                </div>
                              
                                <div class="col-md-6 pr-0">
                                    <div class="form-group ">
                                        <label for="recipient-name" class="col-form-label">Status :</label>
                                        <?php echo $Data->status ?>
                                        <div class="row gutters-xs">
													<div class="col-auto">
														<label class="colorinput">
															<input name="status" type="checkbox" value="OnProgres" <?php if ($Data->status == 'OnProgres' ){echo 'checked';} ?>
                                                            data-toggle="tooltip"  data-original-title="On Progres" class="colorinput-input">
															<span class="colorinput-color bg-primary"></span>
														</label>
													</div>
													<div class="col-auto">
														<label class="colorinput">
															<input name="status" type="checkbox" value="ReadyToTes" <?php if ($Data->status == 'ReadyToTes' ){echo 'checked';} ?>  
                                                            data-toggle="tooltip"  data-original-title="Ready To Tes" class="colorinput-input">
															<span class="colorinput-color bg-secondary"></span>
														</label>
													</div>
													<div class="col-auto">
														<label class="colorinput">
															<input name="status" type="checkbox" value="Testing" <?php if ($Data->status == 'Testing' ){echo 'checked';} ?>  
                                                            data-toggle="tooltip"  data-original-title="Testing" value="info" class="colorinput-input">
															<span class="colorinput-color bg-info"></span>
														</label>
													</div>
													<div class="col-auto">
														<label class="colorinput">
															<input name="status" type="checkbox" value="Issue"  <?php if ($Data->status == 'Issue' ){echo 'checked';} ?>  
                                                            data-toggle="tooltip"  data-original-title="Issue" value="success" class="colorinput-input">
															<span class="colorinput-color bg-success"></span>
														</label>
													</div>
													<div class="col-auto">
														<label class="colorinput">
															<input name="status" type="checkbox" value="Closed"  <?php if ($Data->status == 'Closed' ){echo 'checked';} ?>   
                                                            data-toggle="tooltip"  data-original-title="Closed" class="colorinput-input">
															<span class="colorinput-color bg-danger"></span>
														</label>
													</div>
													<div class="col-auto">
														<label class="colorinput">
															<input name="status" type="checkbox"  value="Sloved" <?php if ($Data->status == 'Sloved' ){echo 'checked';} ?> 
                                                             data-toggle="tooltip"  data-original-title="Sloved" class="colorinput-input">
															<span class="colorinput-color bg-warning"></span>
														</label>
													</div>
												</div>
                                            <!-- <select class="form-control"  name="status" >
                                            <option value="">Pilih Status </option>
                                            <option value="OnProgres">On Progres</option>
                                                    <option <?php if ($Data->status == 'ReadyToTes' ){echo 'selected';} ?>value="ReadyToTes" >Ready To Tes</option>
                                                    <option <?php if ($Data->status == 'Testing' ){echo 'selected';} ?> value="Testing" >Testing</option>
                                                    <option <?php if ($Data->status == 'Issue' ){echo 'selected';} ?>  value="Issue" >Issue</option>
                                                    <option <?php if ($Data->status == 'Sloved' ){echo 'selected';} ?> value="Sloved" >Sloved</option>
                                                    <option <?php if ($Data->status == 'Closed' ){echo 'selected';} ?> value="Closed" >Closed</option> 
                                            </select> -->
       
                                        @if ($errors->has('status'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('status') }}</strong>
                                        </span>
                                        @endif
                                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="col-sm-14">
                                <div class="form-group ">
                                    <label for="recipient-name" class="col-form-label">Keterangan :</label>
                                    <textarea class="form-control" name="keterangan_test" placeholder="Keterangan">{{$Data->keterangan}}</textarea>
                                    @if ($errors->has('role'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('role') }}</strong>
                                    </span>
                                    @endif
                                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                </div>
                                <div class="modal-footer no-bd">
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @endsection