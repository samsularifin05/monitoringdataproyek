@extends('layouts.MasterAdmin')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-center">
                <h4 class="card-title">Form Member</h4>
                <button class="btn btn-primary btn-round ml-auto"
                    onclick="window.location.href = '{{route('member.create')}}';">
                    <i class="fa fa-plus"></i>

                </button>
            </div>
        </div>
        <div class="card-body">
            @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}

            </div><br />
            @endif
            <div class="table-responsive">
                <table id="add-row" class="display table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>No </th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Akses</th>
                            <th><center>Aksi</center></th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        <?php $no = 0;?>

                        @foreach($DataMember as $DataMember )
                        <?php $no++ ;?>
                        <tr>
                            <td>{{$no }}</td>
                            <td>{{$DataMember->name}}</td>
                            <td>{{$DataMember->email}}</td>
                            <td>{{$DataMember->role}}</td>
                            <td align="center">
                                <div class="form-button-action">
                                    <button type="button" data-toggle="tooltip" class="btn btn-link btn-primary"
                                        data-original-title="Edit Project"
                                        onclick="window.location.href = '/member/{{$DataMember->id_users}}/edit';"><i
                                            class="fa fa-edit"></i>
                                    </button>
                                    <form action="{{ route('member.destroy', $DataMember->id_users)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button data-toggle="tooltip" title="" class="btn btn-link btn-danger"
                                            data-original-title="Remove" type="submit">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </form>
                                </div>

                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>



@endsection