@extends('layouts.MasterAdmin')
@section('content')
<div class="page-inner">
    <div class="page-header">
        <h4 class="page-title">New Add Member</h4>
        <ul class="breadcrumbs">
            <li class="nav-home">
                <a href="#">
                    <i class="flaticon-home"></i>
                </a>
            </li>
            <li class="separator">
                <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
                <a href="/FormProject">Form Project</a>
            </li>
            <li class="separator">
                <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
                <a href="/FormProject/create">Add Member</a>
            </li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Informasi Member Yang akan di buat</div>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />@endif
                </div>
                <div class="card-body">
                    <form method="post" action="{{ route('member.store') }}">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group ">
                                        <label for="recipient-name" class="col-form-label">Nama :</label>
                                        <input id="name" placeholder="Nama Lengkap" type="text"
                                            class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                            name="name" value="{{ old('name') }}" required autofocus>
                                        @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                        @endif
                                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                    </div>
                                </div>
                                <div class="col-md-6 pr-0">
                                    <div class="form-group ">
                                        <label for="recipient-name" class="col-form-label">No Telepon :</label>
                                        <input id="no_tlp" placeholder="Nama Lengkap" type="text" type="text"
                                            class="form-control{{ $errors->has('no_tlp') ? ' is-invalid' : '' }}"
                                            name="no_tlp" value="{{ old('no_tlp') }}" required autofocus> 
                                        @if($errors->has('no_tlp'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('no_tlp') }}</strong>
                                        </span>
                                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group ">
                                        <label for="recipient-name" class="col-form-label">Email :</label>
                                        <input id="email" type="email" placeholder="Email"
                                            class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                            name="email" value="{{ old('email') }}" required>
                                        @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                        @endif
                                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                    </div>
                                </div>
                                <div class="col-md-6 pr-0">
                                    <div class="form-group ">
                                        <label for="recipient-name" class="col-form-label">Password :</label>
                                        <input id="password" type="password" placeholder="Password"
                                            class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                            name="password" required>
                                        @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                        @endif
                                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group ">
                                        <label for="recipient-name" class="col-form-label">Konfirmasi Password :</label>
                                        <input id="password-confirm" type="password" placeholder="Re Password"
                                            class="form-control" name="password_confirmation" required> <span
                                            class="glyphicon glyphicon-log-in form-control-feedback"></span> </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group ">
                                    <label for="recipient-name" class="col-form-label">Role :</label>
                                    <select class="form-control{{ $errors->has('role') ? ' is-invalid' : '' }}"
                                        name="role" value="{{ old('role') }}">
                                        <option name="" selected> Pilih Salah Satu </option>
                                        <option name="PM"> PM </option>
                                        <option name="QA"> QA </option>
                                        <option name="Programer"> Programer </option>
                                    </select>
                                    @if ($errors->has('role'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('role') }}</strong>
                                    </span>
                                    @endif
                                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                </div>
                                <div class="modal-footer no-bd">
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @endsection