@extends('layouts.MasterAdmin')
@section('content')
<div class="page-inner">
    <div class="page-header">
        <h4 class="page-title">New Add Project</h4>
        <ul class="breadcrumbs">
            <li class="nav-home">
                <a href="#">
                    <i class="flaticon-home"></i>
                </a>
            </li>
            <li class="separator">
                <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
                <a href="/FormProject">Form Project</a>
            </li>
            <li class="separator">
                <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
                <a href="/FormProject/create">Add Project</a>
            </li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Informasi Project Yang akan di buat</div>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />@endif
                </div>
                <div class="row mt--6">
                <div class="row mt--6">
        
    </div>
    </div>
                <div class="card-body">
                    <form method="post" action="{{ route('FormProject.store') }}">
                    @csrf
                    <div class="col-md-12">
                        <div id="TampilData"> </div>
                    </div>
                        
                        <div class="row">
                            <div class="col-md-6 col-lg-12">
                                <div class="form-group">
                                    {!! Form::label('Nama Project') !!}
                                    {!! Form::text('nama_project', $value = null, ['class' => 'form-control',
                                    'placeholder' => 'Masukan Nama Project']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('Nama Perusahaan') !!}
                                    {!! Form::text('perusahaan', $value = null, ['class' => 'form-control',
                                    'placeholder' => 'Masukan Nama Peusahaan']) !!}
                                </div>

                                <div class="form-group">
                                {!! Form::label('Tanggal Mulai') !!}
                                {!! Form::text('tanggal_mulai', $value = null, ['class' => 'form-control',
                                    'placeholder' => 'Masukan Tanggal Mulai']) !!}
                                </div>
                                <div class="form-group">
                                {!! Form::label('Tanggal Selesai') !!}
                                {!! Form::text('tanggal_selesai', $value = null, ['class' => 'form-control',
                                    'placeholder' => 'Masukan Tanggal Selesai']) !!}
                                </div>
                                <div class="form-group">
                                {!! Form::label('Nama Qa') !!}
                                    <select class="form-control" name="nama" id="PiliData">
                                    <option value=''> Pilih Data Qa</option>
                                        @foreach($DataQa as $Data )
                                        <option value="{{$Data->id_users}}">{{$Data->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <!-- <div class="form-group">
                        <label>Input Date Picker</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="datepicker" name="datepicker">
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                        </div>
                    </div> -->
                                <div class="form-group">
                                    <label for="id_label_multiple">Nama Developer</label>
                                    <select class="form-control reload" name="nama_developer[]" id="PiliData" multiple="multiple">
                                        @foreach($DataMember as $Data )
                                        <option value="{{$Data->id_users}}">{{$Data->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="disableinput">Keteranagn</label>
                                    <textarea name="keterangan" class="form-control"
                                        placeholder="Masukan Keterangan"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="card-action">
                            <button type="submit" class="btn btn-success">Simpan</button>
                            <button class="btn btn-danger"
                                onclick="window.location.href = '/FormProject';">Kembali</button>
                        </div>

                </div>
                </form>
            </div>
        </div>

    </div>
    <script>
        $(document).ready(function () {
            $('#PiliData').on('change',function () {
                var id = $(this).val();
                $.ajax({
                    type: 'GET',
                    url: "/PilihQa/"+id,
                    success: function (respons) {
                        console.log(respons);
                        $('#TampilData').html("");

                        $.each(respons,function (index,element) {
                            $('#TampilData').append(`
                            <input type=hidden name='nope' value='`+element.no_tlp+`'>
                            <input type=hidden name='nama_qa' value='`+element.name+`'>
                            `);
                        });
                    },
                    error: function (e,log) {
                        // DO SOMETHING WHEN ERROR
                    }
                });                
                
            })
        })
    </script>
    @endsection