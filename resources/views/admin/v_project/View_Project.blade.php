@extends('layouts.MasterAdmin')
@section('content')
@include('sweet::alert')

<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-center">
                <h4 class="card-title">Form Project</h4>
                <button class="btn btn-primary btn-round ml-auto"
                    onclick="window.location.href = '{{route('FormProject.create')}}';">
                    <i class="fa fa-plus"></i>
                    
                </button>
            </div>
        </div>
       
        <div class="card-body">
            @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}

            </div><br />
            @endif
            <div class="table-responsive">
                <table id="add-row" class="display table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>No </th>
                            <th>Nama Project</th>
                            <th>Perusahaan</th>
                            <th>Target Selesai</th>
                            <th>Nama QA</th>
                            <th><center>Action</center></th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        <?php $no = 0;?>
                        @foreach($DataProject as $Data )
                        <?php $no++ ;?>
                        <tr>
                            <td>{{$no}}</td>
                            <td>{{$Data->nama_project}}</td>
                            <td>{{$Data->perusahaan}}</td>
                            <td>{{$Data->tanggal_selesai}}</td>
                            <td>{{$Data->nama_qa}}</td>
                            <td align="center">
                                <div class="form-button-action">
                                    <button type="button" data-toggle="tooltip" class="btn btn-link btn-primary"
                                        data-original-title="Edit Project"
                                        onclick="window.location.href = '/FormProject/{{$Data->id_project}}/edit';"><i
                                            class="fa fa-edit"></i>
                                    </button>
                                    <form action="{{ route('FormProject.destroy', $Data->id_project)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button data-toggle="tooltip" title="" class="btn btn-link btn-danger"
                                            data-original-title="Remove" type="submit">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
<script>
</script>

@endsection