@extends('layouts.MasterAdmin')
@section('content')
<div class="page-inner">
					<div class="page-header">
						<h4 class="page-title">New Add Project</h4>
						<ul class="breadcrumbs">
							<li class="nav-home">
								<a href="#">
									<i class="flaticon-home"></i>
								</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="/FormProject">Form Project</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="/FormProject/{{$Data->id_project}}/edit">Edit Data Project</a>
							</li>
						</ul>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="card-title">Informasi Project Yang akan di Ubah</div>
								</div>
								<div class="card-body">
                                <form method="post" action="{{ route('FormProject.update', $Data->id_project) }}">
                                    @method('PATCH')
                                    @csrf
									<div class="row">
										<div class="col-md-6 col-lg-12">
											<div class="form-group">
												<label for="Nama Project">Nama Project</label>
												<input type="text" class="form-control" value="{{ $Data->nama_project }}" name="nama_project" placeholder="Masukan Nama Project">
											</div>
											<div class="form-group">
												<label for="Nama Perusahaan">Nama Perusahaan</label>
												<input type="text" class="form-control" value="{{ $Data->perusahaan }}" name="perusahaan" placeholder="Nama Peusahaan">
											</div>
										
											<div class="form-group">
												<label for="Target Selesai">Tanggal Mulai</label>
												<input type="text" name="tanggal_mulai" value="{{ $Data->tanggal_mulai }}" placeholder="Tanggal Mulai"  class="form-control">
											</div>
											<div class="form-group">
												<label for="Target Selesai">Tanggal Selesai</label>
												<input type="text" name="tanggal_selesai" value="{{ $Data->tanggal_selesai }}" placeholder="Tanggal Selesai"  class="form-control">
											</div>
											<div class="form-group">
												<label for="errorInput">Nama QA</label>
												<input type="text" name="nama_qa" value="{{ $Data->nama_qa }}" placeholder="Masukan Nama QA" class="form-control">
											</div>
											
											<div class="form-group">
											<select class="form-control reload"  name="nama_developer[]" id="id_label_multiple" multiple="multiple">
													@foreach($DataMember as $Data1 )
													<option value="{{ $Data1->id_users }}" 
													@foreach($Data->getIdProject as $t)
													{{ $Data1->id_users == $t->id_users ? 'selected' : '' }} 
													@endforeach > {{ $Data1->name }}</option>
													@endforeach
												</select>
											</div>
                                            <div class="form-group">
												<label for="disableinput">Keteranagn</label>
                                                <textarea name="keterangan" class="form-control" value="{{ $Data->keterangan }}" placeholder="Masukan Keterangan">{{ $Data->keterangan }}</textarea>
											</div>
									</div>
								</div>
								<div class="card-action">
									<button type="submit" class="btn btn-success">Simpan</button>
									<button class="btn btn-danger" onclick="window.location.href = '/FormProject';">Kembali</button>
								</div>
                                
							</div>
                            </form>
						</div>
					</div>
				
				</div>
@endsection
