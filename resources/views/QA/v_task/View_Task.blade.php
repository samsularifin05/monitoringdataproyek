@extends('layouts.MasterQA')
@section('content')
@include('sweet::alert')

<div class="row mt--2">
    <div class="col-md-12">
        <div class="card full-height">
            <div class="card-body">
            @if(session()->get('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}

                </div><br />
                @endif
                <div class="card-title">Pilih Project yang akan di tampilkan</div>
            
                <div class="card-category"></div>
                <select class="form-control" name="id_project" id="PilidData">
                    <option value="">Pilih Data Project</option>
                    @foreach($DataProject as $DataProject )
                    <option value="{{ $DataProject->id_project }}">{{ $DataProject->nama_project }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    </div>
 
    <div class="row mt--6">
        <div class="col-md-12">
            <div id="TampilData"> </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#PilidData').on('change',function () {
                
                var id = $(this).val();
                $.ajax({
                    type: 'GET',
                    url: "/PilihProject/"+id,
                    success: function (respons) {
                        console.log(respons);
                        $('#TampilData').html("");

                        $.each(respons,function (index,element) {
                            var status = '';
                                if (element.status == 'OnProgres') {
                                    cetakstatus = '<span class="colorinput-color bg-primary"  data-toggle="tooltip"  data-original-title="On Progres" class="colorinput-input"> <center style=" margin-top: 5px;"> O </center>';
                                }else if (element.status == 'Testing') {
                                    cetakstatus = '<span class="colorinput-color bg-info"  data-toggle="tooltip"  data-original-title="Testing" class="colorinput-input"><center style=" margin-top: 5px;"> T </center>';
                                }else  if (element.status == 'ReadyToTes') {
                                    cetakstatus = '<span class="colorinput-color bg-secondary"  data-toggle="tooltip"  data-original-title="Ready To Tes" class="colorinput-input"><center style=" margin-top: 5px;"> R </center>';
                                }else  if (element.status == 'Issue') {
                                    cetakstatus = '<span class="colorinput-color bg-success"  data-toggle="tooltip"  data-original-title="Issue" class="colorinput-input"><center style=" margin-top: 5px;"> I </center>';
                                }else  if (element.status == 'Sloved') {
                                    cetakstatus = '<span class="colorinput-color bg-warning" data-toggle="tooltip"  data-original-title="Sloved" class="colorinput-input"><center style=" margin-top: 5px;"> S </center>';
                                }else  if (element.status == 'Closed') {
                                    cetakstatus = '<span class="colorinput-color bg-danger" data-toggle="tooltip"  data-original-title="Closed" class="colorinput-input"><center style=" margin-top: 5px;"> C </center></span>';
                                }
                            
                            $('#TampilData').append(`
                            
                               
                                    <div class="card full-height">
                                        <div class="card-body">
                                        <div class="card-header">
                                                <div class="d-flex align-items-center">
                                                    <h4 class="card-title">`+element.nama_task+`</h4>
                                                    <button class="btn btn-primary btn-round ml-auto"
                                                        onclick="window.location.href = '/FormTest/`+element.id_task+`/edit';">
                                                        <i class="fa fa-check"></i>

                                                    </button>
                                                </div>
                                            </div>
                                            <div class="card-category">`+element.keterangan+`</div>
                                            <div class="d-flex flex-wrap justify-content-around pb-2 pt-2">
                                                <table class="display table table-striped table-hover">
                                                    <tr>
                                                        <td>Kode Task</td>
                                                        <td>Nama Project</td>
                                                        <td>QA</td>
                                                        <td colspan='2'><center>Status</center></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>TP-0`+element.id_task+`</td>
                                                        <td>`+element.nama_project+`</td>
                                                        <td>`+element.name+`</td>
                                                        <td colspan='2'>`+element.status+`</td>
                                                        <td> `+cetakstatus+`</td>
                                                       
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    
                             

                            `);
                        });
                    },
                    error: function (e,log) {
                        // DO SOMETHING WHEN ERROR
                    }
                });                
                
            })
        })
    </script>
@endsection