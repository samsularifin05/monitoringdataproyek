@extends('layouts.MasterQA')
@section('content')
<div class="page-inner">
    <div class="page-header">
        <h4 class="page-title">Applay Project</h4>
        <ul class="breadcrumbs">
            <li class="nav-home">
                <a href="#">
                    <i class="flaticon-home"></i>
                </a>
            </li>
            <li class="separator">
                <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
                <a href="/FormTest">Task Project</a>
            </li>
            <li class="separator">
                <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
                <a href="/Task/create">Apllay New Task</a>
            </li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Informasi Task</div>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />@endif
                </div>
                <div class="card-body">
                
              
                <form method="post" action="{{ route('FormTest.update', $Data->id_task) }}">
                    @method('PATCH')
                    @csrf
                    <div id="TampilData"> </div>

 
                        @foreach ($DataPm as $data)
                            <input type="hidden" name="nope_pm" value="{{$data->no_tlp}}">
                            <input type="hidden" name="nama_pm" value="{{$data->name}}">
                        @endforeach
                        <div class="row">
                                <div class="col-md-6 pr-0">
                                    <div class="form-group ">
                                    <label for="recipient-name" class="col-form-label">Nama Project :</label>
                                    <input type="hidden" name="id_task" value="{{$Data->id_task }}" readonly class=" form-control">
                                    <input type="hidden" name="id_qa" value="{{$Data->getDataUsers->id_users }}" readonly class=" form-control">
                                    <input type="hidden" name="id_project" value="{{$Data->getDataProject->id_project }}" readonly class=" form-control">
                                        <input type="text" name="nama_project" value="{{$Data->getDataProject->nama_project }}" readonly class=" form-control">
                                        @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                        @endif
                                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                    </div>
                                </div>
                            
                               
                                <div class="col-md-6 pr-0">
                                    <div class="form-group ">
                                    <label for="recipient-name" class="col-form-label">Nama Qa :</label>
                                    <!-- <input type="hidden" name="id_users" value="{{$Data->getDataUsers->id_users }}" readonly class=" form-control"> -->
                                    <input type="text" name="nama_qa" value="{{$Data->getDataUsers->name }}" readonly class=" form-control">
                                        @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                        @endif
                                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group ">
                                        <label for="recipient-name" class="col-form-label">Nama Task :</label>
                                        <input id="nama_task" name="nama_task" type="text" readonly placeholder="Nama Task"
                                            class="form-control{{ $errors->has('nama_task') ? ' is-invalid' : '' }}"
                                            name="nama_task" value="{{$Data->nama_task}}" required>
                                        @if ($errors->has('nama_task'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('nama_task') }}</strong>
                                        </span>
                                        @endif
                                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                    </div>
                                </div>
                                <div class="col-md-6 pr-0">
                                    <div class="form-group ">
                                        <label for="recipient-name" class="col-form-label">Status :</label>
                                        <?php echo $Data->status ?>
                                        <div class="row gutters-xs">
													<div class="col-auto">
														<label class="colorinput">
                                                        <input type="hidden" value="{{$Data->status }}" name="status_sebelum">
															<input name="status" type="checkbox" value="OnProgres" <?php if ($Data->status == 'OnProgres' ){echo 'checked';} ?>
                                                            data-toggle="tooltip"  data-original-title="On Progres" class="colorinput-input">
															<span class="colorinput-color bg-primary"></span>
														</label>
													</div>
													<div class="col-auto">
														<label class="colorinput">
															<input name="status" type="checkbox" value="ReadyToTes" <?php if ($Data->status == 'ReadyToTes' ){echo 'checked';} ?>  
                                                            data-toggle="tooltip"  data-original-title="Ready To Tes" class="colorinput-input">
															<span class="colorinput-color bg-secondary"></span>
														</label>
													</div>
													<div class="col-auto">
														<label class="colorinput">
															<input name="status" type="checkbox" value="Testing" <?php if ($Data->status == 'Testing' ){echo 'checked';} ?>  
                                                            data-toggle="tooltip"  data-original-title="Testing" value="info" class="colorinput-input">
															<span class="colorinput-color bg-info"></span>
														</label>
													</div>
													<div class="col-auto">
														<label class="colorinput">
															<input name="status" type="checkbox" value="Issue"  <?php if ($Data->status == 'Issue' ){echo 'checked';} ?>  
                                                            data-toggle="tooltip"  data-original-title="Issue" value="success" class="colorinput-input">
															<span class="colorinput-color bg-success"></span>
														</label>
													</div>
													<div class="col-auto">
														<label class="colorinput">
															<input name="status" type="checkbox" value="Closed"  <?php if ($Data->status == 'Closed' ){echo 'checked';} ?>   
                                                            data-toggle="tooltip"  data-original-title="Closed" class="colorinput-input">
															<span class="colorinput-color bg-danger"></span>
														</label>
													</div>
													<div class="col-auto">
														<label class="colorinput">
															<input name="status" type="checkbox"  value="Sloved" <?php if ($Data->status == 'Sloved' ){echo 'checked';} ?> 
                                                             data-toggle="tooltip"  data-original-title="Sloved" class="colorinput-input">
															<span class="colorinput-color bg-warning"></span>
														</label>
													</div>
												</div>
       
                                        @if ($errors->has('status'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('status') }}</strong>
                                        </span>
                                        @endif
                                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="col-sm-14">
                                <div class="form-group ">
                                    <label for="recipient-name" class="col-form-label">Keterangan :</label>
                                    <textarea class="form-control" readonly name="keterangan" placeholder="Keterangan">{{$Data->keterangan}}</textarea>
                                    @if ($errors->has('role'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('role') }}</strong>
                                    </span>
                                    @endif
                                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                </div>
                              
                            </div>
                        </div>              
               </div>
            </div>
        </div>
        
        <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Test Yang Akan Dibuat</div>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />@endif
                </div>
                <div class="card-body">                               
                        <div class="row">
                                <div class="col-md-6 pr-0">
                                    <div class="form-group ">
                                    <label for="recipient-name" class="col-form-label">Nama Test :</label>
                                        <input type="text" name="nama_test" value="{{$Data->nama_task}}" readonly class=" form-control">
                                        @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                        @endif
                                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                    </div>
                                </div>
                            
                               
                                <div class="col-md-6 pr-0">
                                    <div class="form-group ">
                                    <label for="recipient-name" class="col-form-label">Jenis Test :</label>
                                    <select class="form-control"  name="jenis_test" >   
                                            <option value="" >Pilih Jenis Case</option>
                                            <option value="Positive Case" >Positive Case</option>
                                            <option value="Negative Case" >Negative Case</option>
                                       </select

                                        @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('jenis_test') }}</strong>
                                        </span>
                                        @endif
                                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                    </div>
                                </div>


                               
                                <div class="col-md-6">
                                    <div class="form-group ">
                                        <label for="recipient-name" class="col-form-label">Status</label>
                                        <select class="form-control"  name="status2" >
                                        <option value="ReadyToTes" >Ready To Tes</option>
                                                    <option value="Testing" >Testing</option>
                                                    <option value="Issue" >Issue</option>
                                                    <option value="Solved" >Solved</option>
                                                    <option value="Closed" >Closed</option> 
                                         </select>
                                        @if ($errors->has('nama_task'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('nama_task') }}</strong>
                                        </span>
                                        @endif
                                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="form-group ">
                                    <label for="id_label_multiple">Nama Developer</label>
                                    <select class="form-control " name="id_ussers" id="PiliData" >
                                        @foreach($DataProgramer as $Data )
                                        <option value="{{$Data->id_users}}">{{$Data->name}}</option>
                                        @endforeach
                                    </select>
                                        @if ($errors->has('nama_task'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('nama_task') }}</strong>
                                        </span>
                                        @endif
                                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                    </div>
                                </div>
                                <div class="col-md-12 pr-0">
                                    <div class="form-group ">
                                    <label for="recipient-name" class="col-form-label">Keterangan2 :</label>
                                    <textarea class='form-control TinyEditor' name="keterangan_test" size=120 cols=50 rows=20 placeholder="Keterangan"></textarea>
                                    @if ($errors->has('role'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('role') }}</strong>
                                    </span>
                                    @endif
                                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                    </div>
                                </div>
                               
                            </div>
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Simpan</button>
                        </div>              
               </div>
            </div>
        </div>
 
                 
    <script>
        $(document).ready(function () {
            $('#PiliData').on('change',function () {
                var id = $(this).val();
                $.ajax({
                    type: 'GET',
                    url: "/PilihQa/"+id,
                    success: function (respons) {
                        console.log(respons);
                        $('#TampilData').html("");

                        $.each(respons,function (index,element) {
                            $('#TampilData').append(`
                            <input type=hidden name='nope_programer' value='`+element.no_tlp+`'>
                            <input text=hidden name='nama_programer' value='`+element.name+`'>
                           
                            `);
                        });
                    },
                    error: function (e,log) {
                        // DO SOMETHING WHEN ERROR
                    }
                });                
                
            })
        })
    </script>
    @endsection
