@extends('layouts.MasterQA')
@section('content')

<<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <div class="d-flex align-items-center">
                <h4 class="card-title">Form Member</h4>
                <button class="btn btn-primary btn-round ml-auto"
                    onclick="window.location.href = '{{route('member.create')}}';">
                    <i class="fa fa-plus"></i>

                </button>
            </div>
        </div>
        <div class="card-body">
            @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}

            </div><br />
            @endif
            <div class="table-responsive">
                <table id="add-row" class="display table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>No </th>
                            <th>Type Testing</th>
                            <th>Skenario</th>
                            <th>Result</th>
                            <th>Status</th>
                            <th>
                                <center>Aksi</center>
                            </th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>No </th>
                            <th>Type Testing</th>
                            <th>Skenario</th>
                            <th>Result</th>
                            <th>Status</th>
                            <th>
                                <center>Aksi</center>
                            </th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php $no = 0;?>
                        @foreach($DataTesting as $DataTesting )
                        <?php $no++ ;?>

                        <tr>
                            <td>{{$no}}</td>
                            <td>{{$DataTesting->type_testting}}</td>
                            <td>{{$DataTesting->skenario}}</td>
                            <td>{{$DataTesting->result}}</td>
                            <td>{{$DataTesting->status}}</td>

                            <td>
                                <div class="form-button-action">
                                    <button type="button" data-toggle="tooltip" class="btn btn-link btn-primary"
                                        data-original-title="Edit Project" onclick="window.location.href = ''"><i
                                            class="fa fa-edit"></i>
                                    </button>
                                    <form action="" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button data-toggle="tooltip" title="" class="btn btn-link btn-danger"
                                            data-original-title="Remove" type="submit">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </form>
                                </div>

                            </td>
                        </tr>
                    </tbody>
                </table>
                @endforeach
            </div>
        </div>
    </div>
    </div>


    @endsection