@extends('layouts.MasterQA')
@section('content')
<div class="page-inner">
    <div class="page-header">
        <h4 class="page-title">Edit Test Project</h4>
        <ul class="breadcrumbs">
            <li class="nav-home">
                <a href="#">
                    <i class="flaticon-home"></i>
                </a>
            </li>
            <li class="separator">
                <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
                <a href="/FormTest">Test</a>
            </li>
            <li class="separator">
                <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
                <a href="/Task/create">Edit Test</a>
            </li>
        </ul>
    </div>





    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Edit test </div>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />@endif
                </div>
                <form method="post" action="{{ route('FormTest.update', $DataTesting[0]->id_testing) }}">
                    @method('PATCH')
                    @csrf
                    <input type="hidden" name="ubahdata" value="ubahdata" readonly class=" form-control">
                    <div id="TampilData"> </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6 pr-0">
                                <div class="form-group ">
                                    <label for="recipient-name" class="col-form-label">Nama Test :</label>
                                    <input type="hidden" name="status2" value="{{$DataTesting[0]->status}}" readonly class="form-control">
                                    <input type="hidden" name="kode_testing" value="TST-{{$DataTesting[0]->id_testing}}" readonly class="form-control">
                                    <input type="hidden" name="id_task" value="{{$DataTesting[0]->id_task}}" readonly class=" form-control">
                                    <input type="hidden" name="id_project" value="{{$DataTesting[0]->id_project}}" readonly class="form-control">
                                    <input type="hidden" name="nama_project" value="{{$DataTesting[0]->nama_project}}">
                                    <!-- <input type="hidden" name="id_users" value="{{$DataTesting[0]->id_users}}" readonly -->
                    

                                    <input type="text" name="nama_test" value="{{$DataTesting[0]->nama_task}}"
                                        class=" form-control">
                                    @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                </div>
                            </div>


                            <div class="col-md-6 pr-0">
                                <div class="form-group ">
                                    <label for="recipient-name" class="col-form-label">Jenis Test :</label>
                                    <select class="form-control" name="jenis_test">
                                        <option value="Positive Case"
                                            <?php if ($DataTesting[0]->jenis_test == 'Positive Case' ){echo 'selected';} ?>>
                                            Positive Case</option>
                                        <option value="Negative Case"
                                            <?php if ($DataTesting[0]->jenis_test == 'Negative Case' ){echo 'selected';} ?>>
                                            Negative Case</option>
                                    </select>
                                    @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('jenis_test') }}</strong>
                                    </span>
                                    @endif
                                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                </div>
                            </div>



                            <div class="col-md-6">
                                <div class="form-group ">
                                    <label for="recipient-name" class="col-form-label">Status</label>
                                    <div class="row gutters-xs">
                                        <div class="col-auto">
                                            <label class="colorinput">
                                                <input name="status" type="checkbox" value="OnProgres"
                                                    <?php if ($DataTesting[0]->status_testing == 'OnProgres' ){echo 'checked';} ?>
                                                    data-toggle="tooltip" data-original-title="On Progres"
                                                    class="colorinput-input">
                                                <span class="colorinput-color bg-primary"></span>
                                            </label>
                                        </div>
                                        <div class="col-auto">
                                            <label class="colorinput">
                                                <input name="status" type="checkbox" value="ReadyToTes"
                                                    <?php if ($DataTesting[0]->status_testing == 'ReadyToTes' ){echo 'checked';} ?>
                                                    data-toggle="tooltip" data-original-title="Ready To Tes"
                                                    class="colorinput-input">
                                                <span class="colorinput-color bg-secondary"></span>
                                            </label>
                                        </div>
                                        <div class="col-auto">
                                            <label class="colorinput">
                                                <input name="status" type="checkbox" value="Testing"
                                                    <?php if ($DataTesting[0]->status_testing == 'Testing' ){echo 'checked';} ?>
                                                    data-toggle="tooltip" data-original-title="Testing" value="info"
                                                    class="colorinput-input">
                                                <span class="colorinput-color bg-info"></span>
                                            </label>
                                        </div>
                                        <div class="col-auto">
                                            <label class="colorinput">
                                                <input name="status" type="checkbox" value="Issue"
                                                    <?php if ($DataTesting[0]->status_testing == 'Issue' ){echo 'checked';} ?>
                                                    data-toggle="tooltip" data-original-title="Issue" value="success"
                                                    class="colorinput-input">
                                                <span class="colorinput-color bg-success"></span>
                                            </label>
                                        </div>
                                        <div class="col-auto">
                                            <label class="colorinput">
                                                <input name="status" type="checkbox" value="Closed"
                                                    <?php if ($DataTesting[0]->status_testing == 'Closed' ){echo 'checked';} ?>
                                                    data-toggle="tooltip" data-original-title="Closed"
                                                    class="colorinput-input">
                                                <span class="colorinput-color bg-danger"></span>
                                            </label>
                                        </div>
                                        <div class="col-auto">
                                            <label class="colorinput">
                                                <input name="status" type="checkbox" value="Solved"
                                                    <?php if ($DataTesting[0]->status_testing == 'Solved' ){echo 'checked';} ?>
                                                    data-toggle="tooltip" data-original-title="Solved"
                                                    class="colorinput-input">
                                                <span class="colorinput-color bg-warning"></span>
                                            </label>
                                        </div>
                                    </div>
                                    @if ($errors->has('nama_task'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('nama_task') }}</strong>
                                    </span>
                                    @endif
                                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group ">
                                    <label for="recipient-name" class="col-form-label">Nama Developer</label>
                                    <select class="form-control " name="nama_developer" disabled="true">
                                        @foreach($DataMember as $Data1 )
                                        <option value="{{ $Data1->id_users }}"
                                            {{ $Data1->id_users == $DataTesting[0]->id_ussers ? 'selected' : '' }}>
                                            {{ $Data1->name }}</option>
                                        @endforeach
                                     
                                    </select>
                                    <?php
                                    foreach ($DataMember as $Data1){
                                        if($Data1->id_users == $DataTesting[0]->id_ussers){
                                           echo "<input type='hidden' value='$Data1->name' name='nama_developer'>";
                                        }
                                   }

                                    ?>
                                    <input type="hidden" value="{{ $DataTesting[0]->no_tlp }}" name="nope_programer">
                                    @if ($errors->has('nama_task'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('nama_task') }}</strong>
                                    </span>
                                    @endif
                                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                </div>
                            </div>

                        </div>

                    </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Keterangan</div>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />@endif
                </div>
                <div class="col-md-12 pr-0">
                    <div class="form-group ">
                        <label for="recipient-name" class="col-form-label">Keterangan :</label>
                        <textarea class='form-control TinyEditor' name="keterangan_test" size=120 cols=50 rows=20 placeholder="Keterangan">
                        {{$DataTesting[0]->keterangan_test}}
                        </textarea>
                                   @if ($errors->has('role'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('role') }}</strong>
                        </span>
                        @endif
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary btn-lg btn-block">Simpan</button>
            </div>

        </div>
    </div>
    </form>

    <script>
        $(document).ready(function () {
            $('#PiliData').on('change',function () {
                var id = $(this).val();
                $.ajax({
                    type: 'GET',
                    url: "/PilihQa/"+id,
                    success: function (respons) {
                        console.log(respons);
                        $('#TampilData').html("");

                        $.each(respons,function (index,element) {
                            $('#TampilData').append(`
                            <input type=text name='nope_programer' value='`+element.no_tlp+`'>
                            <input text=text name='nama_programer' value='`+element.name+`'>
                           
                            `);
                        });
                    },
                    error: function (e,log) {
                        // DO SOMETHING WHEN ERROR
                    }
                });                
                
            })
        })
    </script>
    @endsection