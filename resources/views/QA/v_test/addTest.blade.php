@extends('layouts.MasterQA')
@section('content')
<div class="page-inner">
    <div class="page-header">
        <h4 class="page-title">Add New Test</h4>
        <ul class="breadcrumbs">
            <li class="nav-home">
                <a href="#">
                    <i class="flaticon-home"></i>
                </a>
            </li>
            <li class="separator">
                <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
                <a href="/FormTest">Form Test</a>
            </li>
            <li class="separator">
                <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
                <a href="/Task/create">Add New Task</a>
            </li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Informasi Project</div>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />@endif
                </div>
                <div class="card-body">
              
                <form method="post" action="{{ route('FormTest.store', $Data->id_task) }}">
                    @method('PATCH')
                    @csrf

 
                                             
                        <div class="row">
                                <div class="col-md-6 pr-0">
                                    <div class="form-group ">
                                    <label for="recipient-name" class="col-form-label">Nama Project :</label>
                                    <input type="hidden" name="id_task" value="{{$Data->id_task }}" readonly class=" form-control">
                                    <input type="hidden" name="id_project" value="{{$Data->getDataProject->id_project }}" readonly class=" form-control">
                                        <input type="text" value="{{$Data->getDataProject->nama_project }}" readonly class=" form-control">
                                        @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                        @endif
                                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                    </div>
                                </div>
                            
                               
                                <div class="col-md-6 pr-0">
                                    <div class="form-group ">
                                    <label for="recipient-name" class="col-form-label">Nama Qa :</label>
                                    <input type="hidden" name="id_users" value="{{$Data->getDataUsers->id_users }}" readonly class=" form-control">
                                    <input type="text" value="{{$Data->getDataUsers->name }}" readonly class=" form-control">
                                        @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                        @endif
                                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group ">
                                        <label for="recipient-name" class="col-form-label">Nama Task :</label>
                                        <input id="nama_task" type="text" readonly placeholder="Nama Task"
                                            class="form-control{{ $errors->has('nama_task') ? ' is-invalid' : '' }}"
                                            name="nama_task" value="{{$Data->nama_task}}" required>
                                        @if ($errors->has('nama_task'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('nama_task') }}</strong>
                                        </span>
                                        @endif
                                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                    </div>
                                </div>
                                <div class="col-md-6 pr-0">
                                    <div class="form-group ">
                                        <label for="recipient-name" class="col-form-label">Status :</label>
                                            <select class="form-control"  name="status" >
                                            <option <?php if ($Data->status == 'ReadyToTes' ){echo 'selected';} ?>value="ReadyToTes" >Ready To Tes</option>
                                                    <option <?php if ($Data->status == 'Testing' ){echo 'selected';} ?> value="Testing" >Testing</option>
                                                    <option <?php if ($Data->status == 'Issue' ){echo 'selected';} ?>  value="Issue" >Issue</option>
                                                    <option <?php if ($Data->status == 'Solved' ){echo 'selected';} ?> value="Solved" >Solved</option>
                                                    <option <?php if ($Data->status == 'Closed' ){echo 'selected';} ?> value="Closed" >Closed</option> 
                                         </select>
       
                                        @if ($errors->has('status'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('status') }}</strong>
                                        </span>
                                        @endif
                                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="col-sm-14">
                                <div class="form-group ">
                                    <label for="recipient-name" class="col-form-label">Keterangan :</label>
                                    <textarea class="form-control" readonly name="keterangan" placeholder="Keterangan">{{$Data->keterangan}}</textarea>
                                    @if ($errors->has('role'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('role') }}</strong>
                                    </span>
                                    @endif
                                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                </div>
                                
                            </div>
                        </div>              
               </div>
            </div>
        </div>

        <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Test Yang Akan Dibuat</div>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />@endif
                </div>
                <div class="card-body">                               
                        <div class="row">
                                <div class="col-md-6 pr-0">
                                    <div class="form-group ">
                                    <label for="recipient-name" class="col-form-label">Nama Test :</label>
                                        <input type="text" name="nama_test" value=" " class=" form-control">
                                        @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                        @endif
                                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                    </div>
                                </div>
                            
                               
                                <div class="col-md-6 pr-0">
                                    <div class="form-group ">
                                    <label for="recipient-name" class="col-form-label">Jenis Test :</label>
                                    <select class="form-control"  name="jenis_test" >   
                                            <option value="" >Pilih Jenis Case</option>
                                            <option value="Positive Case" >Positive Case</option>
                                            <option value="Negative Case" >Negative Case</option>
                                       </select>

                                        @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('jenis_test') }}</strong>
                                        </span>
                                        @endif
                                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group ">
                                        <label for="recipient-name" class="col-form-label">Status</label>
                                        <select class="form-control"  name="status2" >
                                        <option value="ReadyToTes" >Ready To Tes</option>
                                                    <option value="Testing" >Testing</option>
                                                    <option value="Issue" >Issue</option>
                                                    <option value="Solved" >Solved</option>
                                                    <option value="Closed" >Closed</option> 
                                         </select>
                                        @if ($errors->has('nama_task'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('nama_task') }}</strong>
                                        </span>
                                        @endif
                                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                    </div>
                                </div>
                                <div class="col-md-12 pr-0">
                                    <div class="form-group ">
                                    <label for="recipient-name" class="col-form-label">Keterangan :</label>
                                    <textarea class='TinyEditor' name="keterangan" size=120 cols=50 rows=20 placeholder="Keterangan"></textarea>
                                    @if ($errors->has('role'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('role') }}</strong>
                                    </span>
                                    @endif
                                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                    </div>
                                </div>
                               
                            </div>
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Simpan</button>
                        </div>              
               </div>
            </div>
        </div>
        
    </form>
                 

    @endsection
