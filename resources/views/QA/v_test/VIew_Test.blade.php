@extends('layouts.MasterQA')
@section('content')
<div class="row mt--2">
    <div class="col-md-12">
        <div class="card full-height">
            <div class="card-body">
            @if(session()->get('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}

                </div><br />
                @endif
                <div class="card-title">Pilih Project yang akan di tampilkan</div>
            
                <div class="card-category"></div>
                <select class="form-control" name="id_project" id="PilidData">
                    <option value="">Pilih Data Project</option>
                    @foreach($DataProject as $DataProject )
                    <option value="{{ $DataProject->id_project }}">{{ $DataProject->nama_project }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    </div>
 
    <div class="row mt--6">
        <div class="col-md-12">
            <div id="TampilData"> </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#PilidData').on('change',function () {
                
                var id = $(this).val();
                $.ajax({
                    type: 'GET',
                    url: "/PilihTesting/"+id,
                    success: function (respons) {
                        console.log(respons);
                        $('#TampilData').html("");

                        $.each(respons,function (index,element) {
                            var status_testing = '';
                                if (element.status_testing == 'OnProgres') {
                                    cetakstatus = '<span class="colorinput-color bg-primary"  data-toggle="tooltip"  data-original-title="On Progres" class="colorinput-input"> <center style=" margin-top: 5px;"> O </center>';
                                }else if (element.status_testing == 'Testing') {
                                    cetakstatus = '<span class="colorinput-color bg-info"  data-toggle="tooltip"  data-original-title="Testing" class="colorinput-input"><center style=" margin-top: 5px;"> T </center>';
                                }else  if (element.status_testing == 'ReadyToTes') {
                                    cetakstatus = '<span class="colorinput-color bg-secondary"  data-toggle="tooltip"  data-original-title="Ready To Tes" class="colorinput-input"><center style=" margin-top: 5px;"> R </center>';
                                }else  if (element.status_testing == 'Issue') {
                                    cetakstatus = '<span class="colorinput-color bg-success"  data-toggle="tooltip"  data-original-title="Issue" class="colorinput-input"><center style=" margin-top: 5px;"> I </center>';
                                }else  if (element.status_testing == 'Solved') {
                                    cetakstatus = '<span class="colorinput-color bg-warning" data-toggle="tooltip"  data-original-title="Sloved" class="colorinput-input"><center style=" margin-top: 5px;"> S </center>';
                                }else  if (element.status_testing == 'Closed') {
                                    cetakstatus = '<span class="colorinput-color bg-danger" data-toggle="tooltip"  data-original-title="Closed" class="colorinput-input"><center style=" margin-top: 5px;"> C </center></span>';
                                }
                            
                            $('#TampilData').append(`
                            
                               
                                    <div class="card full-height">
                                        <div class="card-body">
                                        <div class="card-header">
                                                <div class="d-flex align-items-center">
                                                    <h4 class="card-title">`+element.nama_test+`</h4>
                                                    

                                                    </button>
                                                </div>
                                            </div>
                                            <div class="card-category"></div>
                                            <div class="d-flex flex-wrap justify-content-around pb-2 pt-2">
                                                <table class="display table table-striped table-hover">
                                                    <tr>
                                                        <td>Kode Task</td>
                                                        <td>Nama Project</td>
                                                        <td>QA</td>
                                                        <td colspan='2'><center>Status</center></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>TST-0`+element.id_testing+`</td>
                                                        <td>`+element.jenis_test+`</td>
                                                        <td>`+element.status_testing+`</td>
                                                        <td colspan='2'>`+element.status_testing+`</td>
                                                        <td><a href="#" data-id="`+element.id_testing+`"
                                                            data-toggle="modal" data-target="#statusModal" onclick="showStatus(this)">
                                                            `+cetakstatus+`</a 
                                                        </td
                                                 </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    
                             

                            `);
                        });
                    },
                    error: function (e,log) {
                        // DO SOMETHING WHEN ERROR
                    }
                });                
                
            })
        })
    </script>
    
<div class="modal fade" id="statusModal" tabindex="-1" role="dialog" aria-labelledby="statusModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="statusModalLabel">Edit Status</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form method="post" action="/ubahStatusTesting">
         
          @csrf

              <input type="text" id="id_testing" name="id_testing" >
              <input type="text" id="nama_test" name="nama_test" >
              <input type="text" id="nope" name="nope" >
              <input type="text" id="nama_qa" name="nama_qa" >
              <input type="text" id="nama_project" name="nama_project" >
           
           
              <div class="row gutters-xs">
                                        <div class="col-auto">
                                            <label class="colorinput">
                                                <input name="status_testing" type="checkbox" value="OnProgres" id="OnProgres"
                                                    data-toggle="tooltip" data-original-title="On Progres"
                                                    class="colorinput-input">
                                                <span class="colorinput-color bg-primary"></span>
                                            </label>
                                        </div>
                                        <div class="col-auto">
                                            <label class="colorinput">
                                                <input name="status_testing" type="checkbox" value="ReadyToTes" id="ReadyToTes"
                                                  
                                                    data-toggle="tooltip" data-original-title="Ready To Tes"
                                                    class="colorinput-input">
                                                <span class="colorinput-color bg-secondary"></span>
                                            </label>
                                        </div>
                                        <div class="col-auto">
                                            <label class="colorinput">
                                                <input name="status_testing" type="checkbox" value="Testing"  id="Testing"
                                               
                                                    data-toggle="tooltip" data-original-title="Testing" value="info"
                                                    class="colorinput-input">
                                                <span class="colorinput-color bg-info"></span>
                                            </label>
                                        </div>
                                        <div class="col-auto">
                                            <label class="colorinput">
                                                <input name="status_testing" type="checkbox" value="Issue" id="Issue"
                                                    data-toggle="tooltip" data-original-title="Issue" value="success"
                                                    class="colorinput-input">
                                                <span class="colorinput-color bg-success"></span>
                                            </label>
                                        </div>
                                        <div class="col-auto">
                                            <label class="colorinput">
                                                <input name="status_testing" type="checkbox" value="Closed" id="Closed"
                                                  
                                                    data-toggle="tooltip" data-original-title="Closed"
                                                    class="colorinput-input">
                                                <span class="colorinput-color bg-danger"></span>
                                            </label>
                                        </div>
                                        <div class="col-auto">
                                            <label class="colorinput">
                                                <input name="status_testing" type="checkbox" value="Solved" id="Solved"
                                                    data-toggle="tooltip" data-original-title="Solved"
                                                    class="colorinput-input">
                                                <span class="colorinput-color bg-warning"></span>
                                            </label>
                                        </div>
                                    </div>

  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>
<script>
  function showStatus(id) {
      var id_t = $(id).attr('data-id');
      console.log(id_t);
    $.ajax({
        type :'GET',
        url : 'Test/getDetailJson/'+id_t,
        success: function (respons) {
            var jsn = $.parseJSON(respons);
            var sts = jsn.status_testing;
            var nama_test = jsn.nama_test;
            var nope = jsn.no_tlp;
            var nama_qa = jsn.name;
            var nama_project = jsn.nama_project;
            
            $('#id_testing').val(jsn.id_testing).hide();
            $('#nama_test').val(nama_test).hide();
            $('#nope').val(nope).hide();
            $('#nama_qa').val(nama_qa).hide();
            $('#nama_project').val(nama_project).hide();

            if (sts == 'OnProgres'){
                $('#OnProgres').prop('checked', 'true');
            }else if (sts == 'ReadyToTes'){
                $('#ReadyToTes  ').prop('checked', 'true');
            }else if (sts == 'Testing'){
                $('#Testing  ').prop('checked', 'true');
            }else if (sts == 'Issue'){
                $('#Issue  ').prop('checked', 'true');
            }else if (sts == 'Closed'){
                $('#Closed  ').prop('checked', 'true');
            }else if (sts == 'Solved'){
                $('#Solved').prop('checked', 'true');
            }else{
                $('#OnProgres').prop('checked', false);
            }
        }
    })
  }
</script>
@endsection