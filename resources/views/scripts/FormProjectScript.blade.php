<script type="text/javascript" charset="utf-8" async defer>


//ajax header need for deleted and updating data

var table;
var baseUrl=window.location.origin;
//datatables serverSide


$('document').ready(function(){

 $.ajaxSetup({
    beforeSend: function(xhr, type) {
        if (!type.crossDomain) {
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
        }
    },
});
 table = $('#tblData').DataTable({ 
    stateSave: true,
    responsive: true,
    bAutoWidth : false,
    processing: true,
    serverSide : true,
    ajax :  '{{route('FormProject.index')}}',
    columns: [
    {data: 'id_project' , name : 'id_project' },
    {data: 'nama_project' , name : 'nama_project' },
    {data: 'action' , name : 'action', className: "text-right" ,  orderable : false ,searchable: false},
    ],
    "language": {
      "sEmptyTable":   "Tidak ada data klasifikasi yang tersedia pada tabel ini",
      processing : '<span style="width:100%;"><img src="assets/res/img/load.gif" width="200"></span>',
      "sLengthMenu":   "Tampilkan _MENU_ klasifikasi",
      "sZeroRecords":  "Tidak ditemukan data koleksi yang sesuai",
      "sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ klasifikasi",
      "sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 koleksi",
      "sInfoFiltered": "(disaring dari _MAX_ klasifikasi keseluruhan)",
      "sInfoPostFix":  "",
      "sSearch":       "Cari:",
      "sUrl":          "",
      "oPaginate": {
        "sFirst":    "Pertama",
        "sPrevious": "Sebelumnya",
        "sNext":     "Selanjutnya",
        "sLast":     "Terakhir"
      }
    },
});
        // table.draw(false);

//calling add modal 
$('#btnAdd').click(function(e){
   var frm = $('#resetAdd');
   frm.trigger('click');
   $('.errorNama').addClass('d-none');
   $('.errorId').addClass('d-none');
   $('#mdlAddData').modal('show');

});

//Adding new data
$('#btnSave').click(function(e){
    e.preventDefault();
    var frm = $('#frmDataAdd');
    $.ajax({
        url : '{{url('/FormProject')}}',
        type : 'POST',
        dataType: 'json',
        data : {
            'csrf-token': $('input[name=_token]').val(), 
            idKlasifikasi : $('#idKlasifikasi').val(),
            namaKlasifikasi : $('#namaKlasifikasi').val(),
        },
        success:function(data){
            $('.errorNama').addClass('d-none');
            $('.errorId').addClass('d-none');
            if (data.errors) {	
                if (data.errors.idKlasifikasi) {
                    $('.errorId').removeClass('d-none');
                    $('.errorId').text(data.errors.idKlasifikasi);
                }
                if (data.errors.namaKlasifikasi) {
                    $('.errorNama').removeClass('d-none');
                    $('.errorNama').text(data.errors.namaKlasifikasi);
                }
            }
            if (data.success == true) {
                $('#mdlAddData').modal('hide');
                $('.errorNama').addClass('d-none');
                $('.errorId').addClass('d-none');
                frm.trigger('reset');
                table.ajax.reload(null,false);
                swal('success!','Data berhasil ditambahkan','success');
            }

        }
});
});

//calling edit modal and id info of data

$('#tblData').on('click','.btnEdit[data-edit]',function(e){
    e.preventDefault();
    var url = $(this).data('edit');
    swal({
      title: "Apakah Anda Ingin Mengedit Data Ini?",
      type: "info",
      showCancelButton: true,
      confirmButtonClass: "btn-info",
      confirmButtonText: "Ya",
      cancelButtonText: "Tidak",
      closeOnConfirm: true,
      closeOnCancel: true
  }, 
  function(isConfirm) {
    if (isConfirm) {
        $.ajax({
            url : url,
            type : 'GET',
            datatype : 'json',
            success:function(data){
                $('#idKlasifikasiEdit').val(data.kd_klasifikasi);
                $('#namaKlasifikasiEdit').val(data.nm_klasifikasi);
                $('.errorNamaEdit').addClass('d-none');
                $('.errorIdEdit').addClass('d-none');
                $('#mdlEditData').modal('show');
            }

        });
    }
});
});

// updating data infomation
$('#btnUpdate').on('click',function(e){
    e.preventDefault();
    var url = '{{url('/klasifikasi')}}'+'/'+$('#idKlasifikasiEdit').val();
    var frm = $('#frmDataEdit');
    $.ajax({
        type :'PUT',
        url : url,
        dataType : 'json',
        data : {
         'csrf-token': $('input[name=_token]').val(), 
         namaKlasifikasi : $('#namaKlasifikasiEdit').val(),
     },
     success:function(data){
        $('.errorNamaEdit').addClass('d-none');
        $('.errorIdEdit').addClass('d-none');
        if (data.errors) {
            if (data.errors.idKlasifikasi) {
                $('.errorIdEdit').removeClass('d-none');
                $('.errorIdEdit').text(data.errors.idKlasifikasi);
            }
            if (data.errors.namaKlasifikasi) {
                $('.errorNamaEdit').removeClass('d-none');
                $('.errorNamaEdit').text(data.errors.namaKlasifikasi);
            }
        }
        if (data.success == true) {
            $('.edit_errorName').addClass('d-none');
            frm.trigger('reset');
            $('#mdlEditData').modal('hide');
            swal('Success!','Data Updated Successfully','success');
            table.ajax.reload(null,false);
        }
    }
});
});

//deleting data
$('#tblData').on('click','.btnDelete[data-remove]',function(e){
    e.preventDefault();
    var url = $(this).data('remove');
    swal(
    {
       title: "Apakah Anda ingin Menghapus Data ini?",
       text: "Data akan segera di Hapus",
       type: "warning",
       showCancelButton: true,
       confirmButtonClass: "btn-danger",
       confirmButtonText: "Confirm",
       cancelButtonText: "Cancel",
       closeOnConfirm: false,
       closeOnCancel: false,
   },
   function(isConfirm) {
    if (isConfirm) 
    {
        $.ajax({
            url : url,
            type: 'DELETE',
            dataType : 'json',
            data : { method : '_DELETE' , submit : true},
            success:function(data){
                if (data == 'Success') {
                    swal("Terhapus!", "Data Sudah Terhapus", "success");
                    table.ajax.reload(null,false);
                }
            }
        });

    }else{
        swal("Tidak Jadi", "Anda Tidak Jadi menghapus data ini", "error");
    }    

});
});
});

</script>