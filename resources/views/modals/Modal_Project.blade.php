

    <!-- add Data -->
    <div class="modal fade" id="mdlAddData" tabindex="-1" role="dialog" aria-labelledby="mdlAddData" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Project</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                  @if ($errors->any())
                  <div class="alert alert-danger">
                    <ul>
                      @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div><br />
                  @endif
                    <form method="post" action="{{ route('FormProject.store') }}">
                    <div class="form-group">
                      @csrf
                      <label for="recipient-name" class="col-form-label">Nama Project:</label>
                          <input type="text" class="form-control" name="nama_project"/>
                    </div>
                    <div class="form-group">
                      <label for="recipient-name" class="col-form-label">Perusahaan :</label>
                          <input type="text" class="form-control" name="perusahaan"/>
                    </div>
                    <div class="form-group">
                      <label for="recipient-name" class="col-form-label">Target Selesai :</label>
                          <input type="text" class="form-control" name="target_selesai"/>
                    </div>
                    <div class="form-group">
                      <label for="recipient-name" class="col-form-label">Nama QA :</label>
                          <input type="text" class="form-control" name="nama_qa"/>
                    </div>
                    <div class="form-group">
                      <label for="recipient-name" class="col-form-label">Keterangan :</label>
                      <textarea class="form-control" name="keterangan"></textarea>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </div>
                </div>
               
                </div>
                </form></div></div></div></div>
              </section>
            
    <!-- /.add Data --> 


    
    <!-- Edit Data -->
    <div class="modal fade" id="EditFormProject" tabindex="-1" role="dialog" aria-labelledby="EditFormProject" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Form Project</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
               
                <div class="modal-body">
                  @if ($errors->any())
                  <div class="alert alert-danger">
                    <ul>
                      @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div><br />
                  @endif
                    <form method="post" action="{{ route('FormProject.  ') }}">
                    <div class="form-group">
                     @csrf
                      <input type="hidden" class="form-control" Values="$DataProject->id_project" id="id_project" name="id_project"/>
                      <label for="recipient-name" class="col-form-label">Nama Project:</label>
                          <input type="text" class="form-control" Values="" id="nama_project" name="nama_project"/>
                    </div>
                    <div class="form-group">
                      <label for="recipient-name" class="col-form-label">Perusahaan :</label>
                          <input type="text" class="form-control" id="perusahaan" name="perusahaan"/>
                    </div>
                    <div class="form-group">
                      <label for="recipient-name" class="col-form-label">Target Selesai :</label>
                          <input type="text" class="form-control" id="target_selesai" name="target_selesai"/>
                    </div>
                    <div class="form-group">
                      <label for="recipient-name" class="col-form-label">Nama QA :</label>
                          <input type="text" class="form-control" id="nama_qa" name="nama_qa"/>
                    </div>
                    <div class="form-group">
                      <label for="recipient-name" class="col-form-label">Keterangan :</label>
                      <textarea class="form-control" id="keterangan" name="keterangan"></textarea>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </div>
                </div>
              
                </div>
                </form></div></div></div></div>
              </section>
            
    <!-- /.Edit Data --> 
