@extends('layouts.file')
@section('login')
<div class="login-box">
    <div class="login-logo">
        <a href="dashboard"><b>Monitoring </b>Project</a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Silahkan Login Terlebih Dahulu</p>

        <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="form-group has-feedback">
                <input id="email" type="email" placeholder="Masukan Username"
                    class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                    value="{{ old('email') }}" required autofocus>

                @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input id="password" type="password" placeholder="Masukan Password"
                    class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                @if ($errors->has('password'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>

            <div class="social-auth-links text-center">

                <button type="Submit" class="btn btn-block btn-primary btn-flat">
                    {{ __('Login') }}
                </button>

                @if (Route::has('password.request'))
                <a href="{{ route('password.request') }}" class="btn btn-block btn-danger btn-flat"> Lupa Kata Sandi</a>
                @endif

            </div>
            <br>
            <font size="2">
            User Login
                <table border='1'>
                    <tr align="center">
                        <td>Username</td>
                        <td width="200">Password</td>
                        <td width="200"> Akses</td>
                    </tr>
                    <tr align="center">
                        <td>Samsul026@gmail.com</td>
                        <td>bandung5050</td>
                        <td>PM</td>
                    </tr>
                    <tr align="center">
                        <td>sarah@gmail.com</td>
                        <td>bandung5050</td>
                        <td>Programer</td>
                    </tr>
                    <tr align="center">
                        <td>160613058@gmail.com</td>
                        <td>bandung5050</td>
                        <td>QA</td>
                    </tr>
                </table>
            </font>
            <!-- /.social-auth-links -->

        </form>
    </div>
    @endsection