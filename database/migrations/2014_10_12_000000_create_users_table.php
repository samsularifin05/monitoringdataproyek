<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Membuat Table Users
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username')->unique();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('role_name');
            $table->rememberToken();
            $table->timestamps();
        });
        //Membuat Table Roles
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('namaRule');
         });

         //Relasi antar table
        Schema::table('users', function(Blueprint $kolom) {
            $kolom->foreign('roles_id')->references('id')->on('users')->onDelete('cascade')->unUpdate('casecade');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Menghapus table 
        Schema::dropIfExists('users');
        Schema::dropIfExists('roles');

    }
}
