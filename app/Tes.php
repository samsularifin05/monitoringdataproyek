<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tes extends Model
{
    protected $table = 'tbl_testing';
    protected $primaryKey = 'id_testing';
    protected $fillable = ['id_project','id_ussers','id_task','nama_test','jenis_test','status_testing','no_qa','keterangan_test'];
    
    //relasi many to one (Saya adalah member dari project model ......)
    public function getIdProject() {
        return $this->hasMany('App\DetailProject','id_project');
    }
    public function getDataUsers() {
        return $this->hasMany('App\User','id_users');
    }
    public function getIdtask() {
        return $this->hasMany('App\User','id_users');
    }
    
    
}
