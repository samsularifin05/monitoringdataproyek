<?php

namespace App\Http\Controllers;
use App\User;
use App\DataProject;
use App\DataMember;
use App\Task;
use DB;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $DataProject = DataProject::all();
        $DataMember = DataMember::all();
        $ViewProject = DB::table('tbl_project')->limit(4)->get();;
        $Member = DB::table('users')->limit(4)
        ->whereNotIn('role', ['PM'] )->get();;
        $task = Task::sum('id_task');
        // echo '<pre>';
        // print_r($task);
        // echo '<pre>';


        return view('admin/dashboard',compact('DataProject','Member','ViewProject','DataMember'));
    }
}
