<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\DataProject;
use App\User;
use App\DataMember;
use DB;
use \App\Helpers\NotifikasiWa;
use \App\Helpers\LogActivity;
use App\Tes;
Use Alert;
use Illuminate\Support\Facades\Auth;
class TesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(  Auth::user()->role  == "PM") {
            $Data = Task::orderBy('created_at', 'desc')->get()->all();
            return view('admin/v_task/View_Task', compact(['Data'])); 
                  
        }else if(Auth::user()->role  == "Programer"){
            $dataUser = Auth::user()->id_users;
            $DataTest = DB::table('tbl_testing')
             ->join('tbl_project', 'tbl_project.id_project', '=', 'tbl_testing.id_project')
             ->join('users', 'users.id_users', '=', 'tbl_testing.id_ussers')
             ->whereNotIn('tbl_testing.status_testing', ['Closed'] )
             ->join('tbl_task', 'tbl_task.id_task', '=', 'tbl_testing.id_task')
            ->where('tbl_testing.id_ussers', '=', $dataUser)
            ->get();
          //$DataTest = Tes::all();
          return view('Programer/View_Test',['DataTest'=>$DataTest]); 

        }else if(Auth::user()->role  == "QA"){
            $DataProject = DataProject::all();
            $DataTes = DB::table('tbl_testing')
            ->join('tbl_project', 'tbl_project.id_project', '=', 'tbl_testing.id_project')
            ->join('users', 'users.id_users', '=', 'tbl_testing.id_ussers')
            ->whereNotIn('tbl_testing.status_testing', ['Closed'] )
            ->join('tbl_task', 'tbl_task.id_task', '=', 'tbl_testing.id_task')
            ->orderBy('tbl_testing.updated_at', 'desc')
            ->get();
            return view('QA/v_test/View_Test',['DataTes'=>$DataTes],['DataProject'=>$DataProject]); 
        }
        // $Data = DataProject::orderBy('created_at', 'desc')->get();
        // return view('admin/v_project/View_Project',['DataProject' => $Data]); 
    }
    public function PilihTesting($id){
        if($id==0){
            $DataTask = Task::all();
        }else{
            if(Auth::user()->role  == "QA") {
                $dataUser = Auth::user()->id_users;
                $DataTes = DB::table('tbl_testing')
                ->where('id_project', '=', $id)
                ->where('id_ussers', '=', $dataUser)
                ->get();   
            }else{
                $dataUser = Auth::user()->id_users;
                $DataTes = DB::table('tbl_testing')
                ->join('tbl_project', 'tbl_project.id_project', '=', 'tbl_testing.id_project')
                ->join('users', 'users.id_users', '=', 'tbl_testing.id_ussers')
                ->whereNotIn('tbl_testing.status_testing', ['Closed'] )
                ->join('tbl_task', 'tbl_task.id_task', '=', 'tbl_testing.id_task')
                ->where('tbl_testing.id_testing', '=', $id)
                ->where('tbl_testing.id_ussers', '=', $dataUser)
                ->get();
            }
        }
        return $DataTes;
    }

    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $DataProject = DataProject::all();
        $DataMember  = DataMember::where('role','=','QA')->get(); 
        $Data = Task::all();
        return view('QA/v_test/addTest',compact('DataProject', 'Data', 'DataMember')); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $request->validate([
            'id_task'=>'required',
            'id_project'=>'required',
            'id_ussers'=>'required',
            'nama_test'=>'required',
            'jenis_test'=>'required',
            'status'=>'required',
            'status2'=>'required',
            'keterangan_test'=>'required',
        ]);
         $DataTest = new Tes([
            'id_task'=>$request->get('id_task'),
            'id_project'=>$request->get('id_project'),
            'id_ussers'=>$request->get('id_ussers'),
            'nama_test'=>$request->get('nama_test'),
            'jenis_test'=>$request->get('jenis_test'),
            'status2'=>$request->get('status'),
            'no_qa'=>$request->get('no_qa'),
            'keterangan_test'=>$request->get('keterangan_test'), 
        ]);
        
        $DataTask = Task::find($id);
        
        $DataTask->status = $request->get('status');
        $DataTask->save();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
       
        if(Auth::user()->role  == "QA"){
            $Data = Task::find($id);
            $DataMember  = DataMember::where('role','=','Qa')->get();
            $DataPm  = DataMember::where('role','=','PM')->get(); 
            $DataProgramer  = DataMember::where('role','=','Programer')->get(); 
            return view('QA/v_task/editTest',compact(['Data','DataMember','DataPm','DataProgramer'])); 
        }else{
            
            $DataTesting = DB::table('tbl_testing')
            ->join('tbl_project', 'tbl_project.id_project', '=', 'tbl_testing.id_project')
            ->join('users', 'users.id_users', '=', 'tbl_testing.id_ussers')
            ->join('tbl_task', 'tbl_task.id_task', '=', 'tbl_testing.id_task')
            ->where('tbl_testing.id_testing', '=', $id)
            ->get();

            return view('Programer/editTest',['DataTesting'=>$DataTesting]); 

        }
        
    
    }
    public function UbahData($id)
    {
        $DataMember = DataMember::where('role','=','Programer')->get(); 
        $DataTesting = Tes::with('getDataUsers');
        $DataTesting = DB::table('tbl_testing')
        ->join('tbl_project', 'tbl_project.id_project', '=', 'tbl_testing.id_project')
        ->join('tbl_task', 'tbl_task.id_task', '=', 'tbl_testing.id_task')
        ->join('users', 'users.id_users', '=', 'tbl_testing.id_ussers')
        ->where('tbl_testing.id_testing', '=', $id)
        ->get();

        return view('QA/v_test/editTest',['DataTesting'=>$DataTesting],['DataMember'=>$DataMember]); 
    }

    public function statusUbah(Request $request)
    {
        $id = $request->get('id_testing');
        $status_testing = $request->get('status_testing');
        $nama_test = $request->get('nama_test');
        $nope = $request->get('nope');
        $nama_qa = $request->get('nama_qa');
        $nama_project = $request->get('nama_project');

        $DataTesting = Tes::find($id);
        $DataTesting->status_testing =  $status_testing;
        
        // echo $status_testing;
        // echo $nama_task;
        // echo $nope;
        // echo $nama_qa;
        // echo $nama_project;
        $DataTesting->save();
        
  
        $pesan = "*".$nama_qa."*\nTask *_".$nama_test."_* telah di ubah statusnya menjadi *_".$status_testing."_* Oleh Quality Ansurance, untuk project *_".$nama_project."_* ";
        NotifikasiWa::send($nope,$pesan);
        Alert::success("Status ".$nama_test." Berhasil Di Ubah", 'Good Job') ->persistent("Tutup");
        return redirect('/FormTest');
    }


    public function getDetailJson($id)
    {
        header('Content-Type: application/json');
        $Data = DB::table('tbl_testing')
            ->join('tbl_task', 'tbl_task.id_task', '=', 'tbl_testing.id_task')
            ->join('tbl_project', 'tbl_project.id_project', '=', 'tbl_testing.id_project')
            ->join('users', 'users.id_users', '=', 'tbl_testing.id_ussers')
            ->where('tbl_testing.id_testing', '=' ,$id )
           ->get();

        // $Data = Task::find($id);
        echo json_encode($Data[0]);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $request->validate([
            'id_task'=>'required',
            'id_project'=>'required',
            'nama_test'=>'required',
            'jenis_test'=>'required',
        ]);
         $DataTest = new Tes([
            'id_task'=>$request->get('id_task'),
            'id_project'=>$request->get('id_project'),
            'id_ussers'=>$request->get('id_ussers'),
            'nama_test'=>$request->get('nama_test'),
            'jenis_test'=>$request->get('jenis_test'),
            'status_testing'=>$request->get('status2'),
            'no_qa'=>$request->get('no_qa'),
            'keterangan_test'=>$request->get('keterangan_test'),
        ]);
        
        if(Auth::user()->role  == "QA"){
            $ubahdata = $request->get('ubahdata');
            if($ubahdata == "ubahdata"){
                $nope_programer = $request->get('nope_programer');
                $nama_developer = $request->get('nama_developer');
                $nama_test = $request->get('nama_test');
                $nama_project = $request->get('nama_project');
                $status = $request->get('status');
                $Tes = Tes::find($id);
                $Tes->status_testing = $request->get('status');
                $Tes->save();

                $pesan = "".$nama_developer.", Tes _*". $nama_test."*_ telah di ubah statusnya oleh QA menjadi _*".$status."*_  untuk project  _*".$nama_project."*_ ";
                NotifikasiWa::send($nope_programer,$pesan);
                return redirect('/FormTest')->with('success', 'Status Testing Berhasil Di Ubah');;
            }
            $DataTask = Task::find($id);
            $DataTask->status = $request->get('status');

            
            $nope_pm = $request->get('nope_pm');
            $nama_task = $request->get('nama_task');
            $nama_pm = $request->get('nama_pm');
            $nama_qa = $request->get('nama_qa');
            $nama_project = $request->get('nama_project');
            $status = $request->get('status');

            $status_sebelum = $request->get('status_sebelum');
            $nope_programer = $request->get('nope_programer');
            $nama_programer = $request->get('nama_programer');
            $status2 = $request->get('status2');
        
            // Notif Untuk PM
            $pesan = " *".$nama_pm."*, $nama_qa telah mengubah status task  _*".$status_sebelum."*_ menjadi _*".$status."*_  di task _*".$nama_task."*_ untuk project  _*".$nama_project."*_ ";
            NotifikasiWa::send($nope_pm,$pesan);
            $DataTask->save();
            $DataTest->save();
            //Notif Untuk Developer
            $pesan = " *".$nama_programer."*, ada task baru  _*".$nama_task."*_ untuk project _*".$nama_project."*_ dengan status _*".$status2."*_  mohon untuk di cek ";
            NotifikasiWa::send($nope_programer,$pesan);
            return redirect('/FormTest')->with('success', 'Task Berhasil Di Applay');;
        }else{
            $Tes = Tes::find($id);
            $no_qa = $request->get('no_qa');
            $nama_qa = $request->get('nama_qa');
            $nama_test = $request->get('nama_test');
            $nama_project = $request->get('nama_project');
            $status2 = $request->get('status_testing');
            $Tes->status_testing = $request->get('status_testing');
            $nama_dev = Auth::user()->name;
            // echo $no_qa;
            // echo $nama_test;
            // echo  $nama_project;
            // echo $status2;
            // echo $status_testing = $request->get('status_testing');

            $pesan = "task _*".$nama_test."*_ untuk project _*".$nama_project."*_ telah di ubah statusnya oleh developer *".$nama_dev."* menjadi _*".$status2."*_  mohon untuk di cek ";
            NotifikasiWa::send($no_qa,$pesan);
            $Tes->save();
            Alert::success('Status Testing Berhasil Di Ubah', 'Good Job') ->persistent("Tutup");
            return redirect('/FormTest');
        }
        
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Data = Tes::find($id);
        $nama = Auth::user()->name;
        LogActivity::addToLog(''.$nama.', Berhasil menghapus Data Project '.$Data->nama_project.'',$nama);

        $Data->delete();
        Alert::success('Status Testing Berhasil Dihapus', 'Good Job') ->persistent("Tutup");
        return redirect('/FormTest')->with('success', 'Data Project  telah berhasil di hapus');
    }
}
