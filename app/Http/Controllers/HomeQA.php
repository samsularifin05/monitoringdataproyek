<?php

namespace App\Http\Controllers;
use App\User;
use App\DataProject;
use App\DataMember;
use App\Task;
use DB;
use \App\Helpers\NotifikasiWa;
use \App\Helpers\LogActivity;
use Illuminate\Http\Request;

class HomeQA extends Controller
{
    public function index(){
        $DataProject = DataProject::all();
        $DataMember = DataMember::all();
        $ViewProject = DB::table('tbl_project')->limit(4)->get();;
        $Member = DB::table('users')->limit(4)
        ->whereNotIn('role', ['PM'] )->get();
        
        $task = Task::sum('id_task');
        return view('QA.HomeQA',compact('DataProject','Member','ViewProject','DataMember'));
      
    }
}
