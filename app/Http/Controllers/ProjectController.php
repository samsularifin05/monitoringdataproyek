<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\DataProject;
use App\DataMember;
use App\DetailProject;
use Illuminate\Support\Facades\Auth;
use \App\Helpers\LogActivity;
use \App\Helpers\NotifikasiWa;
Use Alert;
use DB;
class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Data = DataProject::orderBy('created_at', 'desc')->get()->all();
        return view('admin/v_project/View_Project',['DataProject' => $Data]); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Data = DataMember::orderBy('created_at', 'desc')->get();
        $DataMember  = DataMember::where('role','=','Programer')->get(); 
        $DataQa  = DataMember::where('role','=','QA')->get(); 
        $DataPm  = DataMember::where('role','=','PM')->get(); 
        return view('admin/v_project/addProject',compact(['Data', 'DataMember','DataQa','DataPm'])); 
    }

    public function PilihQa($id){
        if($id==0){
            $DataUser = DataMember::all();
        }else{
            $DataUser = DataMember::where('id_users','=',$id)->get();
        }
        return $DataUser;
    }
  
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_project'=>'required',
            'perusahaan'=>'required',
            'tanggal_mulai'=>'required',
            'tanggal_selesai'=>'required',
            'nama_qa'=>'required',
        ]);

        $DataProject = new DataProject([      
            'nama_project'=>$request->get('nama_project'),
            'perusahaan'=>$request->get('perusahaan'),
            'tanggal_mulai'=>$request->get('tanggal_mulai'),
            'tanggal_selesai'=>$request->get('tanggal_selesai'),
            'nama_qa'=>$request->get('nama_qa'),
            'keterangan'=>$request->get('keterangan'),    
        ]);

      
        
        $NM_project = $request->get('nama_project');
        $nope = $request->get('nope');
        $nama_qa = $request->get('nama_qa');
        $perusahaan = $request->get('perusahaan');
        
        $DataProject->save();
        $nama = Auth::user()->name;
        LogActivity::addToLog(''.$nama.', Berhasil menyimpan Data Project '.$DataProject->nama_project.'',$nama);
        $users = DB::table('tbl_project')->select('id_project')->orderBy('id_project', 'DESC')->first();
        $users_id = $request->get('nama_developer');       
        if (count($users_id) > 1) {
            for ($i=0; $i < count($users_id); $i++) { 
                $DetailProject = new DetailProject([
                    'id_project'=>$users->id_project,            
                    'id_users'=>$users_id[$i]
                ]);
                $DetailProject->save();  
            }
        }else{
            $DetailProject = new DetailProject([
                'id_project'=>$users->id_project,            
                'id_users'=>$users_id[0]
            ]);
            $DetailProject->save();  
        }
        
        $pesan = " *".$nama_qa."* adnda Telah ditambahkan untuk Project _*".$NM_project."*_ untuk perusahaan _*".$perusahaan."*_ ";
        NotifikasiWa::send($nope,$pesan);
        Alert::success('Project Baru Berhasil Di tambahkan', 'Good Job') ->persistent("Tutup");
        return redirect('/FormProject');
    }


    // /**
    //  * Display the specified resource.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function show($id)
    // {
    //     // $Data = DataProject::find($id);
    //     // $Data->delete();
    //     // return redirect('/FormProject')->with('success', 'Data Project  telah berhasil di hapus');
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
       $DataMember = DataMember::orderBy('created_at', 'desc')->get(); 
       $DataProject = DataProject::with('getIdProject');
       $DataProject = DataProject::find($id);
       return view('admin/v_project/editProject',['Data' => $DataProject],compact('DataMember')); 

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_project'=>'required',
            'perusahaan'=>'required',
            'tanggal_mulai'=>'required',
            'tanggal_selesai'=>'required',
            'nama_qa'=>'required',
            'keterangan'=>'required'
        ]);

           $DataProject = DataProject::find($id);
           $nama = Auth::user()->name;
           LogActivity::addToLog(''.$nama.', Berhasil Mengubah Data Project '.$DataProject->nama_project.'',$nama);

           $DataProject->nama_project = $request->get('nama_project');
           $DataProject->perusahaan = $request->get('perusahaan');
           $DataProject->tanggal_mulai = $request->get('tanggal_mulai');
           $DataProject->tanggal_mulai = $request->get('tanggal_selesai');
           $DataProject->nama_qa = $request->get('nama_qa');
           $DataProject->keterangan = $request->get('keterangan');
           $DataProject->save();
           $users_id = $request->get('nama_developer');       
            if (count($users_id) > 1) {
                for ($i=0; $i < count($users_id); $i++) { 
                    $DetailProject = new DetailProject([
                        'id_project'=>$DataProject->id_project,            
                        'id_users'=>$users_id[$i]
                    ]);
                    $DetailProject->save();  
                }
            }
    
            $NM_project = $request->get('nama_project');
            $nope = $request->get('nope');
            $nama_qa = $request->get('nama_qa');
            $perusahaan = $request->get('perusahaan');
            $pesan = "Selamat <b>".$nama_qa."</b> Telah Terdaftar Di Project *_".$NM_project."_* untuk perusahaan *_".$perusahaan."*_ ";
            NotifikasiWa::send($nope,$pesan);
    
            Alert::success('Data Berhasil Di Ubah', 'Good Job') ->persistent("Tutup");
           return redirect('/FormProject');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Data = DataProject::find($id);
        $nama = Auth::user()->name;
        LogActivity::addToLog(''.$nama.', Berhasil menghapus Data Project '.$Data->nama_project.'',$nama);

        $Data->delete();
        Alert::success('Data Berhasil Di Hapus', 'Good Job') ->persistent("Tutup");
        return redirect('/FormProject');
    }
}
