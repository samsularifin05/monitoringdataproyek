<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\DataProject;
use App\DataMember;
use App\Task;
use DB;
Use Alert;
use \App\Helpers\NotifikasiWa;
use \App\Helpers\LogActivity;

class AdminController extends Controller
{
    public function __construct()
    {


    }

    public function index(){
        // $users = DB::table('tbl_task')->count();
        $DataProject = DataProject::all();
        $DataMember = DataMember::all();
        $ViewProject = DB::table('tbl_project')->limit(4)->get();;
        $Member = DB::table('users')->limit(4)
        ->whereNotIn('role', ['PM'] )->get();
        $logs = DB::table('log_activities')->limit(5)->get();
        $task = Task::sum('id_task');
        // echo '<pre>';
        // print_r($task);
        // echo '<pre>';
       
        return view('admin/dashboard',compact('DataProject','Member','ViewProject','DataMember','logs'));
    }
    // public function Test()
    // {
    //     $project = DB::table('tbl_project')->get();
    //     $data["project"] = array();
    //     $data["total"] = 0;
    //     foreach ($project as $key ) {
    //         $jml = DB::table('tbl_task')
    //         ->select(DB::raw('count(*) as total'))
    //         ->where('tbl_task.id_project','=',$key->id_project)
    //         ->where('tbl_task.status','=', 'Issue')
    //         ->get();
            
    //          array_push($data['project'],array(
    //             'total' => $jml[0]->total,
    //             'nama_project' => $key->nama_project,
    //             'id_project' => $key->id_project
                
    //          ));
    //          $data['total'] += $jml[0]->total;
    //     }
    //     echo json_encode($data);
    //     // echo json_encode($TotalTask);
    // }

    // public function KirimPesan($nope,$pesan){
    //     $DataProject = DataProject::all();
    //     $DataMember = DataMember::all();
    //     $ViewProject = DB::table('tbl_project')->limit(4)->get();;
    //     $Member = DB::table('users')->limit(4)
    //     ->whereNotIn('role', ['PM'] )->get();

    //     NotifikasiWa::send($nope,$pesan);
    //     return view('admin/dashboard',compact('DataProject','Member','ViewProject','DataMember'));
    // }
     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nope = $request->get('nope');
        $pesan = $request->get('pesan');
        $DataProject = DataProject::all();
        $logs = LogActivity::logActivityLists();
            $DataMember = DataMember::all();
            $ViewProject = DB::table('tbl_project')->limit(4)->get();;
            $Member = DB::table('users')->limit(4)
            ->whereNotIn('role', ['PM'] )->get();
    
            NotifikasiWa::send($nope,$pesan);
            Alert::success('Pesan Whatsapp Berhasil Di kirim', 'Pesan Terkirim') ->persistent("Tutup");
            return view('admin/dashboard',compact('DataProject','Member','ViewProject','DataMember','logs'));
    }
    public function PilihDataProject($id){
        if($id==0){
            $DataTask = Task::all();
        }else{
            // $task = DB::table('tbl_task')
            // ->join('tbl_project', 'tbl_project.id_project', '=', 'tbl_task.id_project')
            // ->join('users', 'users.id_users', '=', 'tbl_task.id_users')
            // ->select('tbl_task.status','tbl_project.nama_project', DB::raw('count(*) as total'))
            // ->where('tbl_task.id_project','=',$id)
            // ->groupBy('tbl_project.nama_project','tbl_task.status')
            // ->get();

            $task = DB::table('tbl_task')
            ->join('tbl_project', 'tbl_project.id_project', '=', 'tbl_task.id_project')
            ->join('users', 'users.id_users', '=', 'tbl_task.id_users')
            ->select('status', DB::raw('count(*) as total'))
            ->where('tbl_task.id_project','=',$id)
            ->groupBy('status')
            ->pluck('total','status')->all();
        }
        //  echo '<pre>';
        // print_r($task);
        // echo '<pre>';
        return $task;
    }

    // public function myTestAddToLog()
    // {
    //     LogActivity::addToLog('My Testing Add To Log.');
    //     dd('log insert successfully.');
    // }

    public function logActivity()
    {
        $logs = LogActivity::logActivityLists();
        return view('admin/log',compact('logs'));
    }

    public function destroy($id)
    {
        $Data = LogActivity::find($id);
        $Data->delete();
        return redirect('admin/log')->with('success', 'Data Project  telah berhasil di hapus');
    }
}

