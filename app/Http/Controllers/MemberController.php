<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Crypt;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\DataMember;
use Illuminate\Support\Facades\Hash;
use \App\Helpers\LogActivity;
use Illuminate\Support\Facades\Auth;
use \App\Helpers\NotifikasiWa;
use DB;
Use Alert;
class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Jumlah = DB::table('users')->count('id_users');
        $Data = DataMember::orderBy('created_at', 'desc')->get();
        
        return view('admin/v_member/View_Member',['DataMember'=> $Data],['Jumlah'=> $Jumlah]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('admin/v_member/addMember');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'no_tlp' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'role' => 'required|in:PM,QA,Programer',
        ]);

        $DataMember = new DataMember([
            'name'=>$request->get('name'),
            'no_tlp'=>$request->get('no_tlp'),
            'email'=>$request->get('email'),
            'password' => Hash::make($request['password']),
            'role'=>$request->get('role'),
        ]);
        
        $nope = $request->get('no_tlp');
        $nama = $request->get('name');
        $role = $request->get('role');
        $email = $request->get('email');

        
        
        $DataMember->save();
        if(  Auth::user()->role  == "PM") {
            $nama = Auth::user()->name;
        }
        LogActivity::addToLog(''.$nama.', Berhasil Menambah Member Baru',$nama);

        $pesan = "Selamat Anda Telah Terdaftar Di Aplikasi Monitoring Project dengan data berikut :\nNama : ".$nama."\nJabatan : ".$role."\nEmail  : ".$email."";
        NotifikasiWa::send($nope,$pesan);
        Alert::success('Berhasil Menambah Member Baru', 'Good Job') ->persistent("Tutup");
         return redirect('/member');
    }

    // /**
    //  * Display the specified resource.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function show($id)
    // {
    //     //
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Data = DataMember::find($id);
        return view('admin/v_member/editmember', compact('Data'));  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'no_tlp' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'role' => 'required|in:PM,QA,Programer',
        ]);
       
        $DataMember = new DataMember([
            'name'=>$request->get('name'),
            'no_tlp'=>$request->get('no_tlp'),
            'email'=>$request->get('email'),
            'password' => Hash::make($request['password']),
            'role'=>$request->get('role'),
        ]);
        
        $DataMember->save();
        if(  Auth::user()->role  == "PM") {
            $nama = Auth::user()->name;
        }
        LogActivity::addToLog(''.$nama.', Berhasil Mengubah Data Member',$nama);

        return redirect('/member')->with('success','Project Baru Berhasil Perbaharui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Data = DataMember::find($id);
        $Data->delete();
        if(  Auth::user()->role  == "PM") {
            $nama = Auth::user()->name;
        }
        LogActivity::addToLog(''.$nama.', Berhasil Menghapus Data',$nama);

        return redirect('/member')->with('success', 'Data Project  telah berhasil di hapus');
   }
}
