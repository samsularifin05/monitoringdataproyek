<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Task;
use App\DataProject;
use App\User;
use App\DataMember;
use \App\Helpers\LogActivity;
use \App\Helpers\NotifikasiWa;
use DB;
Use Alert;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(  Auth::user()->role  == "PM") {
            $Data = DB::table('tbl_task')
            ->join('tbl_project', 'tbl_project.id_project', '=', 'tbl_task.id_project')
            ->join('users', 'users.id_users', '=', 'tbl_task.id_users')
           ->get();
            // $Data = Task::orderBy('created_at', 'desc')->get()->all();
            return view('admin/v_task/View_Task',['Data'=>$Data]); 
            // return view('admin/v_task/View_Task', compact(['Data']));     

        }else if(Auth::user()->role  == "Programer"){
            $jabatan = "Programer";
        }else if(Auth::user()->role  == "QA"){
            $DataProject = DataProject::all();
            $dataUser = Auth::user()->id_users;
            $DataTask = DB::table('tbl_task')
             ->join('tbl_project', 'tbl_project.id_project', '=', 'tbl_task.id_project')
             ->join('users', 'users.id_users', '=', 'tbl_task.id_users')
             ->whereNotIn('tbl_task.status', ['Closed'] )
            ->where('tbl_task.id_users', '=', $dataUser)
            ->get();
            return view('QA/v_task/View_Task',['DataTask'=>$DataTask],['DataProject'=>$DataProject]); 

            // $DataProject = DataProject::all();
            // $DataUsers = Task::with('getDataUsers')->get();
            // $Data = Task::with('getDataProject')->get();
            // return view('Qa/v_task/View_Task',['Data' => $Data],['DataProject' => $DataProject],['DataUsers' => $DataUsers]); 
        }
              
    }

    public function getDetailJson($id)
    {
        header('Content-Type: application/json');
        $Data = DB::table('tbl_task')
            ->join('tbl_project', 'tbl_project.id_project', '=', 'tbl_task.id_project')
            ->join('users', 'users.id_users', '=', 'tbl_task.id_users')
            ->where('tbl_task.id_task', '=' ,$id )
           ->get();

        // $Data = Task::find($id);
        echo json_encode($Data[0]);
    }

    public function PilihProject($id){
        if($id==0){
            $DataTask = Task::all();
        }else{
           $dataUser = Auth::user()->id_users;
            $DataTask = DB::table('tbl_task')
            ->join('tbl_project', 'tbl_project.id_project', '=', 'tbl_task.id_project')
            ->join('users', 'users.id_users', '=', 'tbl_task.id_users')
            ->whereNotIn('tbl_task.status', ['Closed'] )
            ->where('tbl_task.id_project', '=', $id)
            ->where('users.id_users', '=', $dataUser)
            ->get();
        }
        return $DataTask;
    }
    public function ProjectDetail($id){
        if($id==0){
            $DataUser = DataProject::all();
        }else{
            $DataUser = DataProject::where('id_project','=',$id)->get();
        }
        return $DataUser;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $DataMember  = DataMember::where('role','=','QA')->get(); 
        $DataProject = DataProject::all();
        $Task        = Task::all();
        return view('admin/v_task/addTask',compact('DataProject', 'Task', 'DataMember')); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'id_project'=>'required',
            'id_users'=>'required',
            'nama_task'=>'required',
            'status'=>'required',
            'keterangan'=>'required',
        ]);

        $Task = new Task([
            'id_project'=>$request->get('id_project'),
            'id_users'=>$request->get('id_users'),
            'nama_task'=>$request->get('nama_task'),
            'status'=>$request->get('status'),
            'keterangan'=>$request->get('keterangan'), 
        ]);
        $nama_project = $request->get('nama_project');
        $nope = $request->get('nope');
        $nama_qa = $request->get('nama_qa');
        $nama_task = $request->get('nama_task');
        $status = $request->get('status');

        $nama = Auth::user()->name;
        LogActivity::addToLog(''.$nama.', Berhasil Menyimpan Task '.$Task->nama_task.' ',$nama);
        $pesan = "*".$nama_qa."*</b> ada task baru yang harus di kerjakan pada Project *_".$nama_project."_* dengan nama task *_".$nama_task."_* dan status *_".$status."_* ";
        NotifikasiWa::send($nope,$pesan);

        $Task->save();
        Alert::success('Task Baru Berhasil Disimpan', 'Good Job') ->persistent("Tutup");
        return redirect('/Task');
    }

    // /**
    //  * Display the specified resource.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function show($id)
    // {
    //     return view('admin/v_task/editTask');
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $DataProject = DataProject::all();
        $DataMember  = DataMember::where('role','=','QA')->get(); 
        $Data = Task::find($id);
        
        return view('admin/v_task/editTask',compact(['DataProject', 'DataMember', 'Data'])); 

        
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function statusUbah(Request $request)
    {
        $id = $request->get('id_task');
        $status = $request->get('status');
        $nama_task = $request->get('nama_task');
        $nope = $request->get('nope');
        $nama_qa = $request->get('nama_qa');
        $nama_project = $request->get('nama_project');

        $DataTask = Task::find($id);
        $DataTask->status =  $status;
        $DataTask->save();
        
  
        $pesan = "*".$nama_qa."*\nTask *_".$nama_task."_* telah di ubah statusnya menjadi *_".$status."_* Oleh Project Manager, untuk project *_".$nama_project."_* ";
        NotifikasiWa::send($nope,$pesan);
        Alert::success("Status ".$nama_task." Berhasil Di Ubah", 'Good Job') ->persistent("Tutup");
        return redirect('/Task');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $DataTask = Task::find($id);
           $DataTask->id_project = $request->get('id_project');
           $DataTask->id_users = $request->get('id_users');
           $DataTask->nama_task = $request->get('nama_task');
           $DataTask->status = $request->get('status');
           $DataTask->keterangan = $request->get('keterangan');


           $nama_project = $request->get('nama_project');
           $no_qa = $request->get('no_qa');
           $nama_qa = $request->get('nama_qa');
           $nama_task = $request->get('nama_task');
           $status = $request->get('status');
           
           $nama = Auth::user()->name;
           LogActivity::addToLog(''.$nama.', Berhasil Mengubah Data '.$DataTask->nama_task.' untuk id project '.$DataTask->id_users.' ',$nama);
          
           $pesan = "*".$nama_qa."* Task *_".$nama_task."_* telah di ubah statusnya menjadi *_".$status."_* Oleh Project Manager";
           NotifikasiWa::send($no_qa,$pesan);
           $DataTask->save();
           Alert::success('Task Baru Berhasil Diupdate', 'Good Job') ->persistent("Tutup");
           return redirect('/Task');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Data = Task::find($id);
        $nama = Auth::user()->name;
        LogActivity::addToLog(''.$nama.', Berhasil Menghapus Data '.$Data->nama_task.'',$nama);
        $Data->delete();
        Alert::success('Task Baru Berhasil Dihapus', 'Good Job') ->persistent("Tutup");
        return redirect('/Task');
    }
}
