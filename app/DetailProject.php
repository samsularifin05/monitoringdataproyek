<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailProject extends Model
{
    protected $table = "tbl_detail_project";
    protected $primaryKey = 'id';
    protected $fillable = ['id_project','id_users'];
   
    public function getIdProject() 
    {
        return $this->hasMany('App\DataProject', 'id_project');
    }
    
 
}
