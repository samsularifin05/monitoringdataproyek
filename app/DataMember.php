<?php
 
namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Notifications\ResetPasswordNotification;
use Illuminate\Foundation\Auth\User as Authenticatable;
 
class DataMember extends Authenticatable
{
    use Notifiable;

    protected $table = "users";
    protected $primaryKey = 'id_users';
    protected $fillable = ['name','no_tlp','email','password','role'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function getRememberToken(){
        return $this->remember_token;
    }
    public function setRememberToken($value){
        $this->remember_token = $value;
    }
    public function getRememberTokenName(){
        return 'remember_token';
    }
    
}