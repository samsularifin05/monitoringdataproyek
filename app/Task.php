<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table = 'tbl_task';
    protected $primaryKey = 'id_task';
    protected $fillable = ['id_project','id_users','nama_task','status','keterangan'];
    
    //relasi many to one (Saya adalah member dari project model ......)
    public function getDataProject() {
        return $this->belongsTo('App\DataProject','id_project');
    }
    public function getDataUsers() {
        return $this->belongsTo('App\User','id_users');
    }
    
    
    
    
}
