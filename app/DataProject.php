<?php
 
namespace App;
use DB;
use App\DataProject;
use Illuminate\Database\Eloquent\Model;
class DataProject extends Model
{
    protected $table = "tbl_project";
    protected $primaryKey = 'id_project';
    protected $fillable = ['nama_project','perusahaan','tanggal_mulai','tanggal_selesai','nama_qa','nama_developer','keterangan'];

    //relasi one to many (Saya memiliki banyak member di model Task)
    public function getIdProject() {
        return $this->hasMany('App\DetailProject','id_project');
    }
    
    public function getUsers() 
    {
        return $this->hasMany('App\User', 'id_users');
    }

}
    



