<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes(['verify' => true]);
Route::get('/', function () {
	return view('login.FormLogin');
});



Auth::routes();
//Admin
Route::get('/home', 'HomeController@index')->name('home')->middleware(['auth']);
Route::get('/member', 'MemberController@index')->name('member')->middleware(['auth']);
Route::get('/delete/{id}','MemberController@delete');
//

Route::get('/EditDataProject', function () {
	return view('admin.EditDataProject');
});

Route::resource('/member', 'MemberController')->middleware(['auth']);
Route::resource('/FormProject', 'ProjectController')->middleware(['auth']);
Route::resource('/DataTesting', 'DataTestingController')->middleware(['auth']);
Route::resource('/Task', 'TaskController')->middleware(['auth']);
Route::resource('/FormTest', 'TesController')->middleware(['auth']);
Route::resource('/PM', 'AdminController')->middleware(['auth']);

Route::get('/Programer', 'HomeProgramer@index')->name('Programer')->middleware(['auth']);
Route::get('/PM', 'AdminController@index')->name('PM')->middleware(['auth']);
Route::get('/QA', 'HomeQA@index')->name('QA')->middleware(['auth']);

route::get('/Task/{id_project}','TaskController@DataTask');

route::get('/Task/getDetailJson/{id}','TaskController@getDetailJson');

route::get('/Test/getDetailJson/{id}','TesController@getDetailJson');

Route::get('/logActivity', 'AdminController@logActivity');
Route::get('/PilihProject/{id_project}', 'TaskController@PilihProject');
Route::get('/PilihDataProject/{id_project}', 'AdminController@PilihDataProject');

Route::post('/ubahStatus', 'TaskController@statusUbah')->name('Task.ubahStatus');
Route::post('/ubahStatusTesting', 'TesController@statusUbah')->name('Test.ubahStatus');


Route::get('/PilihTesting/{id_project}', 'TesController@PilihTesting');
Route::get('/FormTest/{id_testing}/UbahData', 'TesController@UbahData');


Route::get('/simpanUbah', 'TesController@simpanUbah')->name('Test.simpanUbah');

Route::get('/PilihQa/{id_users}', 'ProjectController@PilihQa');
Route::get('/ProjectDetail/{id_project}', 'TaskController@ProjectDetail');

Route::get('/test', 'AdminController@Test');

Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});