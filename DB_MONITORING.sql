/*
SQLyog Ultimate v12.5.1 (32 bit)
MySQL - 10.1.38-MariaDB : Database - db_monitoring
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_monitoring` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `db_monitoring`;

/*Table structure for table `log_activities` */

DROP TABLE IF EXISTS `log_activities`;

CREATE TABLE `log_activities` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `subject` text,
  `url` text,
  `method` text,
  `ip` text,
  `agent` text,
  `nama_users` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=191 DEFAULT CHARSET=latin1;

/*Data for the table `log_activities` */

insert  into `log_activities`(`id`,`subject`,`url`,`method`,`ip`,`agent`,`nama_users`,`created_at`,`updated_at`) values 
(22,'Samsul Arifin, Berhasil menghapus Data Project Management Project123','http://127.0.0.1:8000/FormProject/56','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36','Samsul Arifin','2019-08-01 16:08:17','2019-08-01 16:08:17'),
(23,'Samsul Arifin, Berhasil menyimpan Data Project samsul Arifin','http://127.0.0.1:8000/FormProject','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36','Samsul Arifin','2019-08-01 16:10:25','2019-08-01 16:10:25'),
(24,'Samsul Arifin, Berhasil Mengubah Data Project samsul Arifin','http://127.0.0.1:8000/FormProject/58','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36','Samsul Arifin','2019-08-01 16:10:47','2019-08-01 16:10:47'),
(25,'Samsul Arifin, Berhasil Mengubah Data Membuat Dashboard Guru untuk project 7 ','http://127.0.0.1:8000/Task/5','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36','Samsul Arifin','2019-08-01 16:18:20','2019-08-01 16:18:20'),
(26,'Samsul Arifin, Berhasil Menyimpan Data Membuat Login Mahasiswa ','http://127.0.0.1:8000/Task','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36','Samsul Arifin','2019-08-01 16:35:17','2019-08-01 16:35:17'),
(27,'Samsul Arifin, Berhasil Menghapus Data ','http://127.0.0.1:8000/Task/5','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36','Samsul Arifin','2019-08-01 16:36:17','2019-08-01 16:36:17'),
(28,'Samsul Arifin, Berhasil Menghapus Data Membuat Login Mahasiswa','http://127.0.0.1:8000/Task/6','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36','Samsul Arifin','2019-08-01 16:37:04','2019-08-01 16:37:04'),
(29,'Samsul Arifin, Berhasil Mengubah Data Membuat Login untuk id project 4 ','http://127.0.0.1:8000/Task/7','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36','Samsul Arifin','2019-08-01 16:37:32','2019-08-01 16:37:32'),
(30,'Samsul Arifin, Berhasil menyimpan Data Project samsul Arifin','http://127.0.0.1:8000/FormProject','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36','Samsul Arifin','2019-08-05 13:06:20','2019-08-05 13:06:20'),
(31,'Samsul Arifin, Berhasil Menyimpan Task Membuat Dashboard ','http://127.0.0.1:8000/Task','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-07 04:27:12','2019-08-07 04:27:12'),
(32,'Samsul Arifin, Berhasil Mengubah Data Membuat Login untuk id project 4 ','http://127.0.0.1:8000/Task/7','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-07 05:08:01','2019-08-07 05:08:01'),
(33,'Samsul Arifin, Berhasil Menyimpan Task Membuat Login Mahasiswa ','http://127.0.0.1:8000/Task','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-07 05:08:17','2019-08-07 05:08:17'),
(34,'Samsul Arifin, Berhasil Menyimpan Task Membuat Dashboard Mahasiswa ','http://127.0.0.1:8000/Task','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-07 13:23:25','2019-08-07 13:23:25'),
(35,'Samsul Arifin, Berhasil Menambah Member Baru','http://127.0.0.1:8000/member','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-07 14:23:14','2019-08-07 14:23:14'),
(36,'Samsul Arifin, Berhasil Menambah Member Baru','http://127.0.0.1:8000/member','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-07 14:26:37','2019-08-07 14:26:37'),
(37,'Samsul Arifin, Berhasil Menambah Member Baru','http://127.0.0.1:8000/member','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-07 14:27:51','2019-08-07 14:27:51'),
(38,'Samsul Arifin, Berhasil Menambah Member Baru','http://127.0.0.1:8000/member','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-07 14:34:42','2019-08-07 14:34:42'),
(39,'Samsul Arifin, Berhasil Menghapus Data','http://127.0.0.1:8000/member/28','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-07 14:39:57','2019-08-07 14:39:57'),
(40,'Samsul Arifin, Berhasil Menghapus Data','http://127.0.0.1:8000/member/29','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-07 14:40:00','2019-08-07 14:40:00'),
(41,'Samsul Arifin, Berhasil Menghapus Data','http://127.0.0.1:8000/member/27','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-07 14:40:02','2019-08-07 14:40:02'),
(42,'Samsul Arifin, Berhasil Menghapus Data','http://127.0.0.1:8000/member/26','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-07 14:40:04','2019-08-07 14:40:04'),
(43,'Samsul Arifin, Berhasil Menambah Member Baru','http://127.0.0.1:8000/member','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-07 14:40:26','2019-08-07 14:40:26'),
(44,'Rizki, Berhasil Menambah Member Baru','http://127.0.0.1:8000/member','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Rizki','2019-08-07 14:42:00','2019-08-07 14:42:00'),
(45,'Rizki, Berhasil Menambah Member Baru','http://127.0.0.1:8000/member','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Rizki','2019-08-07 14:42:46','2019-08-07 14:42:46'),
(46,'Rizki, Berhasil Menambah Member Baru','http://127.0.0.1:8000/member','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Rizki','2019-08-07 14:43:27','2019-08-07 14:43:27'),
(47,'Rizki, Berhasil Menambah Member Baru','http://127.0.0.1:8000/member','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Rizki','2019-08-07 14:44:17','2019-08-07 14:44:17'),
(48,'Samsul Arifin, Berhasil Menambah Member Baru','http://127.0.0.1:8000/member','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-07 14:53:05','2019-08-07 14:53:05'),
(49,'Samsul Arifin, Berhasil menyimpan Data Project Aplikasi Data Monitoring Project Suteki','http://127.0.0.1:8000/FormProject','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-07 15:35:29','2019-08-07 15:35:29'),
(50,'Samsul Arifin, Berhasil menghapus Data Project Aplikasi Data Monitoring Project Suteki','http://127.0.0.1:8000/FormProject/56','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-07 15:39:06','2019-08-07 15:39:06'),
(51,'Samsul Arifin, Berhasil menyimpan Data Project Aplikasi Data Monitoring Suteki','http://127.0.0.1:8000/FormProject','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-07 15:39:56','2019-08-07 15:39:56'),
(52,'Samsul Arifin, Berhasil menyimpan Data Project Program Akutansi','http://127.0.0.1:8000/FormProject','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-07 15:45:07','2019-08-07 15:45:07'),
(53,'Samsul Arifin, Berhasil menyimpan Data Project samsul Arifin','http://127.0.0.1:8000/FormProject','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-07 15:51:30','2019-08-07 15:51:30'),
(54,'Samsul Arifin, Berhasil menghapus Data Project samsul Arifin','http://127.0.0.1:8000/FormProject/59','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-07 15:53:40','2019-08-07 15:53:40'),
(55,'Samsul Arifin, Berhasil menghapus Data Project Program Akutansi','http://127.0.0.1:8000/FormProject/58','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-07 15:53:41','2019-08-07 15:53:41'),
(56,'Samsul Arifin, Berhasil menghapus Data Project Aplikasi Data Monitoring Suteki','http://127.0.0.1:8000/FormProject/57','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-07 15:53:42','2019-08-07 15:53:42'),
(57,'Samsul Arifin, Berhasil menyimpan Data Project samsul Arifin','http://127.0.0.1:8000/FormProject','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-07 15:55:10','2019-08-07 15:55:10'),
(58,'Samsul Arifin, Berhasil menyimpan Data Project samsul Arifin','http://127.0.0.1:8000/FormProject','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-07 15:57:35','2019-08-07 15:57:35'),
(59,'Samsul Arifin, Berhasil menghapus Data Project samsul Arifin','http://127.0.0.1:8000/FormProject/61','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-07 15:59:44','2019-08-07 15:59:44'),
(60,'Samsul Arifin, Berhasil menyimpan Data Project Program Akutansi','http://127.0.0.1:8000/FormProject','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-07 16:00:12','2019-08-07 16:00:12'),
(61,'Samsul Arifin, Berhasil menghapus Data Project Program Akutansi','http://127.0.0.1:8000/FormProject/62','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-07 16:00:55','2019-08-07 16:00:55'),
(62,'Samsul Arifin, Berhasil menyimpan Data Project samsul Arifin','http://127.0.0.1:8000/FormProject','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-07 16:01:10','2019-08-07 16:01:10'),
(63,'Samsul Arifin, Berhasil menghapus Data Project samsul Arifin','http://127.0.0.1:8000/FormProject/63','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-07 16:02:21','2019-08-07 16:02:21'),
(64,'Samsul Arifin, Berhasil menyimpan Data Project samsul Arifin','http://127.0.0.1:8000/FormProject','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-07 16:06:04','2019-08-07 16:06:04'),
(65,'Samsul Arifin, Berhasil menghapus Data Project samsul Arifin','http://127.0.0.1:8000/FormProject/64','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-07 16:06:08','2019-08-07 16:06:08'),
(66,'Samsul Arifin, Berhasil menyimpan Data Project Program Akutansi Akutansi','http://127.0.0.1:8000/FormProject','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-07 16:06:36','2019-08-07 16:06:36'),
(67,'Samsul Arifin, Berhasil menyimpan Data Project Program Akutansi','http://127.0.0.1:8000/FormProject','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-07 16:08:45','2019-08-07 16:08:45'),
(68,'Samsul Arifin, Berhasil Menyimpan Task Membuat Home3232 ','http://127.0.0.1:8000/Task','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-10 00:30:05','2019-08-10 00:30:05'),
(69,'Samsul Arifin, Berhasil Menyimpan Task Membuat Home3232 ','http://127.0.0.1:8000/Task','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-10 00:30:17','2019-08-10 00:30:17'),
(70,'Samsul Arifin, Berhasil Menyimpan Task Membuat Home3232 ','http://127.0.0.1:8000/Task','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-10 00:30:24','2019-08-10 00:30:24'),
(71,'Samsul Arifin, Berhasil Menyimpan Task Membuat Home3232 ','http://127.0.0.1:8000/Task','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-10 07:31:08','2019-08-10 07:31:08'),
(72,'Samsul Arifin, Berhasil Menyimpan Task Membuat Home3232 ','http://127.0.0.1:8000/Task','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-10 07:31:59','2019-08-10 07:31:59'),
(73,'Samsul Arifin, Berhasil Menyimpan Task Membuat Dashboard Mahasiswa ','http://127.0.0.1:8000/Task','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-10 08:20:59','2019-08-10 08:20:59'),
(74,'Samsul Arifin, Berhasil Menyimpan Task 56` ','http://127.0.0.1:8000/Task','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-10 08:25:15','2019-08-10 08:25:15'),
(75,'Samsul Arifin, Berhasil Menyimpan Task Membuat Dashboard ','http://127.0.0.1:8000/Task','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-10 08:25:35','2019-08-10 08:25:35'),
(76,'Samsul Arifin, Berhasil Menyimpan Task Membuat Home3232 ','http://127.0.0.1:8000/Task','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-10 08:37:08','2019-08-10 08:37:08'),
(77,'Samsul Arifin, Berhasil Menghapus Data Membuat Login','http://127.0.0.1:8000/Task/7','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-10 01:42:00','2019-08-10 01:42:00'),
(78,'Samsul Arifin, Berhasil Menghapus Data 56`','http://127.0.0.1:8000/Task/12','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-10 01:42:04','2019-08-10 01:42:04'),
(79,'Samsul Arifin, Berhasil Menyimpan Task Samsul Arifin ','http://127.0.0.1:8000/Task','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-10 08:42:25','2019-08-10 08:42:25'),
(80,'Samsul Arifin, Berhasil Menyimpan Task asd ','http://127.0.0.1:8000/Task','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-10 08:48:10','2019-08-10 08:48:10'),
(81,'Samsul Arifin, Berhasil Menyimpan Task asd ','http://127.0.0.1:8000/Task','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-10 08:48:39','2019-08-10 08:48:39'),
(82,'Samsul Arifin, Berhasil Menyimpan Task Samsul Arifin ','http://127.0.0.1:8000/Task','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-10 08:49:18','2019-08-10 08:49:18'),
(83,'Samsul Arifin, Berhasil Menyimpan Task Samsul Arifin ','http://127.0.0.1:8000/Task','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-10 08:50:44','2019-08-10 08:50:44'),
(84,'Samsul Arifin, Berhasil Menyimpan Task Samsul Arifin ','http://127.0.0.1:8000/Task','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-10 08:51:07','2019-08-10 08:51:07'),
(85,'Samsul Arifin, Berhasil Menyimpan Task Samsul Arifin ','http://127.0.0.1:8000/Task','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-10 01:51:38','2019-08-10 01:51:38'),
(86,'Samsul Arifin, Berhasil Menambah Member Baru','http://127.0.0.1:8000/member','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-10 01:57:43','2019-08-10 01:57:43'),
(87,'Samsul Arifin, Berhasil Menambah Member Baru','http://127.0.0.1:8000/member','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-10 01:58:24','2019-08-10 01:58:24'),
(88,'Samsul Arifin, Berhasil Mengubah Data Membuat Login Mahasiswa Manage untuk id project 31 ','http://127.0.0.1:8000/Task/9','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-10 12:05:24','2019-08-10 12:05:24'),
(89,'Samsul Arifin, Berhasil Mengubah Data Membuat Dashboard Mahasiswa untuk id project 31 ','http://127.0.0.1:8000/Task/11','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-10 12:05:40','2019-08-10 12:05:40'),
(90,'Samsul Arifin, Berhasil Mengubah Data Membuat Dashboard Mahasiswa untuk id project 31 ','http://127.0.0.1:8000/Task/11','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-10 12:05:48','2019-08-10 12:05:48'),
(91,'Samsul Arifin, Berhasil Mengubah Data Membuat Dashboard Mahasiswa untuk id project 7 ','http://127.0.0.1:8000/Task/10','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-10 12:10:54','2019-08-10 12:10:54'),
(92,'Samsul Arifin, Berhasil Mengubah Data Membuat Login Mahasiswa Manage untuk id project 31 ','http://127.0.0.1:8000/Task/9','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-10 12:11:02','2019-08-10 12:11:02'),
(93,'Samsul Arifin, Berhasil Menyimpan Task Membuat Login ','http://127.0.0.1:8000/Task','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 01:35:24','2019-08-11 01:35:24'),
(94,'Samsul Arifin, Berhasil Menghapus Data Membuat Dashboard','http://127.0.0.1:8000/Task/8','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 01:39:33','2019-08-11 01:39:33'),
(95,'Samsul Arifin, Berhasil Menyimpan Task Membuat Laporan Penjualan ','http://127.0.0.1:8000/Task','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 01:40:02','2019-08-11 01:40:02'),
(96,'Samsul Arifin, Berhasil Mengubah Data Samsul Arifin untuk id project 31 ','http://127.0.0.1:8000/Task/15','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 01:41:38','2019-08-11 01:41:38'),
(97,'Samsul Arifin, Berhasil Mengubah Data Membuat Home3232 untuk id project 4 ','http://127.0.0.1:8000/Task/14','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 01:41:48','2019-08-11 01:41:48'),
(98,'Samsul Arifin, Berhasil Mengubah Data Membuat Dashboard untuk id project 31 ','http://127.0.0.1:8000/Task/13','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 01:41:56','2019-08-11 01:41:56'),
(99,'Samsul Arifin, Berhasil Mengubah Data Membuat Laporan Penjualan untuk id project 4 ','http://127.0.0.1:8000/Task/18','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 01:43:17','2019-08-11 01:43:17'),
(100,'Samsul Arifin, Berhasil Mengubah Data Membuat Login untuk id project 4 ','http://127.0.0.1:8000/Task/17','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 01:43:23','2019-08-11 01:43:23'),
(101,'Samsul Arifin, Berhasil Mengubah Data Membuat Laporan Penjualan untuk id project 4 ','http://127.0.0.1:8000/Task/18','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 01:43:29','2019-08-11 01:43:29'),
(102,'Samsul Arifin, Berhasil Mengubah Data Membuat Laporan Penjualan untuk id project 4 ','http://127.0.0.1:8000/Task/18','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 01:44:42','2019-08-11 01:44:42'),
(103,'Sam Ar, Berhasil menyimpan Data Project Aplikasi Data Pegawai','http://127.0.0.1:8000/FormProject','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Sam Ar','2019-08-11 02:49:14','2019-08-11 02:49:14'),
(104,'Sam Ar, Berhasil Mengubah Data Project Aplikasi Data Pegawai','http://127.0.0.1:8000/FormProject/67','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Sam Ar','2019-08-11 02:50:01','2019-08-11 02:50:01'),
(105,'Samsul Arifin, Berhasil Menyimpan Task Membuat Login ','http://127.0.0.1:8000/Task','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 04:42:46','2019-08-11 04:42:46'),
(106,'Samsul Arifin, Berhasil Menyimpan Task Samsul Gan ','http://127.0.0.1:8000/Task','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 04:44:56','2019-08-11 04:44:56'),
(107,'Samsul Arifin, Berhasil Menyimpan Task asd ','http://127.0.0.1:8000/Task','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 04:47:01','2019-08-11 04:47:01'),
(108,'Samsul Arifin, Berhasil Menyimpan Task Membuat Login Mahasiswa ','http://127.0.0.1:8000/Task','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 04:47:33','2019-08-11 04:47:33'),
(109,'Samsul Arifin, Berhasil Menghapus Data Samsul Gan','http://127.0.0.1:8000/Task/20','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 05:49:24','2019-08-11 05:49:24'),
(110,'Samsul Arifin, Berhasil Mengubah Data Membuat Laporan Penjualan untuk id project 4 ','http://127.0.0.1:8000/Task/18','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 05:50:39','2019-08-11 05:50:39'),
(111,'Samsul Arifin, Berhasil Mengubah Data Membuat Laporan Penjualan untuk id project 4 ','http://127.0.0.1:8000/Task/18','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 05:51:11','2019-08-11 05:51:11'),
(112,'Samsul Arifin, Berhasil Mengubah Data Samsul Arifin untuk id project 31 ','http://127.0.0.1:8000/Task/15','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 05:52:35','2019-08-11 05:52:35'),
(113,'Samsul Arifin, Berhasil Mengubah Data Samsul Arifin untuk id project 31 ','http://127.0.0.1:8000/Task/15','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 05:52:52','2019-08-11 05:52:52'),
(114,'Samsul Arifin, Berhasil Mengubah Data Samsul Arifin untuk id project 31 ','http://127.0.0.1:8000/Task/15','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 05:53:38','2019-08-11 05:53:38'),
(115,'Samsul Arifin, Berhasil Mengubah Data Samsul Arifin untuk id project 31 ','http://127.0.0.1:8000/Task/15','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 05:53:50','2019-08-11 05:53:50'),
(116,'Samsul Arifin, Berhasil Menghapus Data Membuat Login Mahasiswa Manage','http://127.0.0.1:8000/Task/9','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 09:26:56','2019-08-11 09:26:56'),
(117,'Samsul Arifin, Berhasil Menghapus Data Samsul Arifin','http://127.0.0.1:8000/Task/16','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 10:12:20','2019-08-11 10:12:20'),
(118,'Samsul Arifin, Berhasil Mengubah Data Samsul Arifin untuk id project 31 ','http://127.0.0.1:8000/Task/15','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 10:14:53','2019-08-11 10:14:53'),
(119,'Samsul Arifin, Berhasil Menghapus Data Samsul Arifin','http://127.0.0.1:8000/Task/15','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 10:15:05','2019-08-11 10:15:05'),
(120,'Samsul Arifin, Berhasil Mengubah Data Membuat Dashboard Mahasiswa untuk id project 31 ','http://127.0.0.1:8000/Task/11','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 10:15:15','2019-08-11 10:15:15'),
(121,'Samsul Arifin, Berhasil Mengubah Data Membuat Dashboard Mahasiswa untuk id project 31 ','http://127.0.0.1:8000/Task/11','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 10:16:13','2019-08-11 10:16:13'),
(122,'Samsul Arifin, Berhasil Mengubah Data Membuat Login untuk id project 31 ','http://127.0.0.1:8000/Task/19','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 10:17:13','2019-08-11 10:17:13'),
(123,'Samsul Arifin, Berhasil Mengubah Data Membuat Login untuk id project 4 ','http://127.0.0.1:8000/Task/17','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 10:18:15','2019-08-11 10:18:15'),
(124,'Samsul Arifin, Berhasil Mengubah Data Membuat Laporan Penjualan untuk id project 4 ','http://127.0.0.1:8000/Task/18','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 10:18:23','2019-08-11 10:18:23'),
(125,'Samsul Arifin, Berhasil Mengubah Data Membuat Laporan Penjualan untuk id project 4 ','http://127.0.0.1:8000/Task/18','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 10:19:31','2019-08-11 10:19:31'),
(126,'Samsul Arifin, Berhasil Mengubah Data Membuat Login Mahasiswa untuk id project 31 ','http://127.0.0.1:8000/Task/21','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 10:19:41','2019-08-11 10:19:41'),
(127,'Samsul Arifin, Berhasil Mengubah Data Membuat Login Mahasiswa untuk id project 31 ','http://127.0.0.1:8000/Task/21','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 10:19:56','2019-08-11 10:19:56'),
(128,'Samsul Arifin, Berhasil Menyimpan Task Membuat Home3232 ','http://127.0.0.1:8000/Task','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 10:21:50','2019-08-11 10:21:50'),
(129,'Samsul Arifin, Berhasil Menyimpan Task Membuat Login ','http://127.0.0.1:8000/Task','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 10:22:12','2019-08-11 10:22:12'),
(130,'Samsul Arifin, Berhasil Menyimpan Task asd ','http://127.0.0.1:8000/Task','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 10:22:31','2019-08-11 10:22:31'),
(131,'Samsul Arifin, Berhasil Mengubah Data asd untuk id project 4 ','http://127.0.0.1:8000/Task/24','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 10:25:51','2019-08-11 10:25:51'),
(132,'Samsul Arifin, Berhasil Mengubah Data asd untuk id project 4 ','http://127.0.0.1:8000/Task/24','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 10:26:02','2019-08-11 10:26:02'),
(133,'Samsul Arifin, Berhasil Mengubah Data Membuat Login Mahasiswa untuk id project 31 ','http://127.0.0.1:8000/Task/21','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 10:29:00','2019-08-11 10:29:00'),
(134,'Samsul Arifin, Berhasil Mengubah Data Membuat Login untuk id project 7 ','http://127.0.0.1:8000/Task/23','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 10:29:15','2019-08-11 10:29:15'),
(135,'Samsul Arifin, Berhasil Mengubah Data Membuat Home3232 untuk id project 7 ','http://127.0.0.1:8000/Task/22','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 10:29:29','2019-08-11 10:29:29'),
(136,'Samsul Arifin, Berhasil Mengubah Data Membuat Login Mahasiswa untuk id project 31 ','http://127.0.0.1:8000/Task/21','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 10:29:36','2019-08-11 10:29:36'),
(137,'Samsul Arifin, Berhasil Mengubah Data Membuat Login untuk id project 31 ','http://127.0.0.1:8000/Task/19','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 10:29:52','2019-08-11 10:29:52'),
(138,'Samsul Arifin, Berhasil Mengubah Data Membuat Login Mahasiswa untuk id project 31 ','http://127.0.0.1:8000/Task/21','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 10:29:59','2019-08-11 10:29:59'),
(139,'Samsul Arifin, Berhasil Mengubah Data Membuat Home3232 untuk id project 7 ','http://127.0.0.1:8000/Task/22','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 10:30:07','2019-08-11 10:30:07'),
(140,'Samsul Arifin, Berhasil Mengubah Data Membuat Login untuk id project 7 ','http://127.0.0.1:8000/Task/23','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 10:30:14','2019-08-11 10:30:14'),
(141,'Samsul Arifin, Berhasil Mengubah Data asd untuk id project 4 ','http://127.0.0.1:8000/Task/24','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 10:30:20','2019-08-11 10:30:20'),
(142,'Samsul Arifin, Berhasil Mengubah Data Membuat Login untuk id project 4 ','http://127.0.0.1:8000/Task/17','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 10:31:39','2019-08-11 10:31:39'),
(143,'Samsul Arifin, Berhasil Menghapus Data Membuat Login','http://127.0.0.1:8000/Task/17','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 10:31:44','2019-08-11 10:31:44'),
(144,'Samsul Arifin, Berhasil Mengubah Data Membuat Dashboard untuk id project 31 ','http://127.0.0.1:8000/Task/13','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 10:31:53','2019-08-11 10:31:53'),
(145,'Samsul Arifin, Berhasil Mengubah Data Membuat Home3232 untuk id project 4 ','http://127.0.0.1:8000/Task/14','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 10:33:14','2019-08-11 10:33:14'),
(146,'Samsul Arifin, Berhasil Mengubah Data asd untuk id project 4 ','http://127.0.0.1:8000/Task/24','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 12:28:05','2019-08-11 12:28:05'),
(147,'Samsul Arifin, Berhasil Mengubah Data Membuat Login untuk id project 7 ','http://127.0.0.1:8000/Task/23','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 12:28:21','2019-08-11 12:28:21'),
(148,'Samsul Arifin, Berhasil Mengubah Data Membuat Home3232 untuk id project 7 ','http://127.0.0.1:8000/Task/22','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 12:28:31','2019-08-11 12:28:31'),
(149,'Samsul Arifin, Berhasil Mengubah Data Membuat Login Mahasiswa untuk id project 31 ','http://127.0.0.1:8000/Task/21','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 12:28:37','2019-08-11 12:28:37'),
(150,'Samsul Arifin, Berhasil Mengubah Data Membuat Login untuk id project 31 ','http://127.0.0.1:8000/Task/19','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 12:28:45','2019-08-11 12:28:45'),
(151,'Samsul Arifin, Berhasil Mengubah Data Membuat Laporan Penjualan untuk id project 4 ','http://127.0.0.1:8000/Task/18','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 12:29:00','2019-08-11 12:29:00'),
(152,'Samsul Arifin, Berhasil Mengubah Data asd untuk id project 4 ','http://127.0.0.1:8000/Task/24','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-11 12:31:53','2019-08-11 12:31:53'),
(153,'Samsul Arifin, Berhasil Mengubah Data Project Aplikasi Data Pegawai','http://127.0.0.1:8000/FormProject/67','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-16 15:58:03','2019-08-16 15:58:03'),
(154,'Samsul Arifin, Berhasil Mengubah Data Project Aplikasi Data Pegawai','http://127.0.0.1:8000/FormProject/67','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-16 15:58:25','2019-08-16 15:58:25'),
(155,'Samsul Arifin, Berhasil Mengubah Data Project Aplikasi Data Pegawai','http://127.0.0.1:8000/FormProject/67','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-16 15:59:25','2019-08-16 15:59:25'),
(156,'Samsul Arifin, Berhasil Mengubah Data Project Aplikasi Data Pegawai','http://127.0.0.1:8000/FormProject/67','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-16 16:02:00','2019-08-16 16:02:00'),
(157,'Samsul Arifin, Berhasil menghapus Data Project samsul Arifin','http://127.0.0.1:8000/FormProject/60','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-16 16:04:33','2019-08-16 16:04:33'),
(158,'Samsul Arifin, Berhasil menghapus Data Project samsul Arifin','http://127.0.0.1:8000/FormProject/60','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-16 16:05:01','2019-08-16 16:05:01'),
(159,'Samsul Arifin, Berhasil menghapus Data Project Program Akutansi Akutansi','http://127.0.0.1:8000/FormProject/65','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-16 16:05:19','2019-08-16 16:05:19'),
(160,'Samsul Arifin, Berhasil menghapus Data Project Program Akutansi Akutansi','http://127.0.0.1:8000/FormProject/65','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-16 16:05:32','2019-08-16 16:05:32'),
(161,'Samsul Arifin, Berhasil menghapus Data Project Program Akutansi Akutansi','http://127.0.0.1:8000/FormProject/65','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-16 16:06:18','2019-08-16 16:06:18'),
(162,'Samsul Arifin, Berhasil menghapus Data Project Program Akutansi Akutansi','http://127.0.0.1:8000/FormProject/65','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-16 16:06:56','2019-08-16 16:06:56'),
(163,'Sam Ar, Berhasil menghapus Data Project ','http://127.0.0.1:8000/FormTest/16','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Sam Ar','2019-08-17 15:03:13','2019-08-17 15:03:13'),
(164,'Sam Ar, Berhasil menghapus Data Project ','http://127.0.0.1:8000/FormTest/25','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Sam Ar','2019-08-17 18:32:06','2019-08-17 18:32:06'),
(165,'Samsul Arifin, Berhasil Menyimpan Task Login ','http://127.0.0.1:8000/Task','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36 OPR/62.0.3331.116','Samsul Arifin','2019-08-17 18:33:03','2019-08-17 18:33:03'),
(166,'Sam Ar, Berhasil menghapus Data Project ','http://127.0.0.1:8000/FormTest/35','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Sam Ar','2019-08-18 02:13:48','2019-08-18 02:13:48'),
(167,'Sam Ar, Berhasil menghapus Data Project ','http://127.0.0.1:8000/FormTest/36','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Sam Ar','2019-08-18 02:20:52','2019-08-18 02:20:52'),
(168,'Sam Ar, Berhasil menghapus Data Project ','http://127.0.0.1:8000/FormTest/37','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Sam Ar','2019-08-18 02:20:54','2019-08-18 02:20:54'),
(169,'Sam Ar, Berhasil menghapus Data Project ','http://127.0.0.1:8000/FormTest/38','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Sam Ar','2019-08-18 02:20:56','2019-08-18 02:20:56'),
(170,'Sam Ar, Berhasil menghapus Data Project ','http://127.0.0.1:8000/FormTest/39','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Sam Ar','2019-08-18 02:22:15','2019-08-18 02:22:15'),
(171,'Samsul Arifin, Berhasil menghapus Data Project Penjualan','http://127.0.0.1:8000/FormProject/54','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-18 04:11:22','2019-08-18 04:11:22'),
(172,'Samsul Arifin, Berhasil menghapus Data Project Program Akutansi','http://127.0.0.1:8000/FormProject/55','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-18 04:11:24','2019-08-18 04:11:24'),
(173,'Samsul Arifin, Berhasil menghapus Data Project Program Akutansi Akutansi','http://127.0.0.1:8000/FormProject/56','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-18 04:11:26','2019-08-18 04:11:26'),
(174,'Samsul Arifin, Berhasil menghapus Data Project Program Akutansi','http://127.0.0.1:8000/FormProject/57','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-18 04:11:28','2019-08-18 04:11:28'),
(175,'Samsul Arifin, Berhasil menyimpan Data Project Aplikasi Data Servis','http://127.0.0.1:8000/FormProject','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-18 04:12:53','2019-08-18 04:12:53'),
(176,'Samsul Arifin, Berhasil menyimpan Data Project Membuat Anggi Bahagia','http://127.0.0.1:8000/FormProject','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-18 04:15:34','2019-08-18 04:15:34'),
(177,'Samsul Arifin, Berhasil menghapus Data Project Membuat Anggi Bahagia','http://127.0.0.1:8000/FormProject/60','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-18 04:17:43','2019-08-18 04:17:43'),
(178,'Samsul Arifin, Berhasil menghapus Data Project Membuat Anggi Bahagia','http://127.0.0.1:8000/FormProject/60','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-18 04:18:06','2019-08-18 04:18:06'),
(179,'Samsul Arifin, Berhasil menyimpan Data Project Service Data BNT','http://127.0.0.1:8000/FormProject','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-18 04:18:43','2019-08-18 04:18:43'),
(180,'Samsul Arifin, Berhasil menghapus Data Project Service Data BNT','http://127.0.0.1:8000/FormProject/61','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-18 04:20:21','2019-08-18 04:20:21'),
(181,'Samsul Arifin, Berhasil menghapus Data Project Aplikasi Data Servis','http://127.0.0.1:8000/FormProject/59','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-18 04:20:24','2019-08-18 04:20:24'),
(182,'Samsul Arifin, Berhasil menyimpan Data Project Management Project','http://127.0.0.1:8000/FormProject','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-18 04:20:42','2019-08-18 04:20:42'),
(183,'Samsul Arifin, Berhasil menghapus Data Project Management Project','http://127.0.0.1:8000/FormProject/62','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-18 04:22:01','2019-08-18 04:22:01'),
(184,'Samsul Arifin, Berhasil menyimpan Data Project Program Akutansi','http://127.0.0.1:8000/FormProject','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-18 04:22:21','2019-08-18 04:22:21'),
(185,'Samsul Arifin, Berhasil menghapus Data Project Program Akutansi','http://127.0.0.1:8000/FormProject/63','DELETE','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-18 04:24:56','2019-08-18 04:24:56'),
(186,'Samsul Arifin, Berhasil menyimpan Data Project Program Akutansi','http://127.0.0.1:8000/FormProject','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-18 04:25:15','2019-08-18 04:25:15'),
(187,'Samsul Arifin, Berhasil Menyimpan Task Membuat Login ','http://127.0.0.1:8000/Task','POST','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-18 04:26:32','2019-08-18 04:26:32'),
(188,'Samsul Arifin, Berhasil Mengubah Data Membuat Login untuk id project 4 ','http://127.0.0.1:8000/Task/16','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-18 04:31:45','2019-08-18 04:31:45'),
(189,'Samsul Arifin, Berhasil Mengubah Data Membuat Login untuk id project 4 ','http://127.0.0.1:8000/Task/16','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-18 04:38:19','2019-08-18 04:38:19'),
(190,'Samsul Arifin, Berhasil Mengubah Data Membuat Login untuk id project 4 ','http://127.0.0.1:8000/Task/16','PATCH','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36','Samsul Arifin','2019-08-18 04:39:43','2019-08-18 04:39:43');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(1,'2014_10_12_000000_create_users_table',1),
(2,'2014_10_12_100000_create_password_resets_table',1),
(3,'2019_03_14_084910_create_user_roles_table',2),
(4,'2014_03_14_130600_create_roles_table',3),
(5,'2019_08_07_164902_create_notifications_table',4);

/*Table structure for table `notifications` */

DROP TABLE IF EXISTS `notifications`;

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notifiable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) unsigned NOT NULL,
  `data` text COLLATE utf8_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `notifications` */

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `tbl_detail_project` */

DROP TABLE IF EXISTS `tbl_detail_project`;

CREATE TABLE `tbl_detail_project` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_project` int(20) DEFAULT NULL,
  `id_users` int(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_project` (`id_project`)
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_detail_project` */

insert  into `tbl_detail_project`(`id`,`id_project`,`id_users`,`created_at`,`updated_at`) values 
(67,55,6,'2019-07-25 04:56:01','2019-07-25 04:56:01'),
(68,55,4,'2019-07-25 04:56:01','2019-07-25 04:56:01'),
(91,54,1,'2019-07-25 06:46:29','2019-07-25 06:46:29'),
(92,56,6,'2019-08-01 10:25:25','2019-08-01 10:25:25'),
(93,56,6,'2019-08-01 10:30:17','2019-08-01 10:30:17'),
(94,56,1,'2019-08-01 10:30:17','2019-08-01 10:30:17'),
(95,56,6,'2019-08-01 15:43:13','2019-08-01 15:43:13'),
(96,56,1,'2019-08-01 15:43:13','2019-08-01 15:43:13'),
(97,56,6,'2019-08-01 15:43:48','2019-08-01 15:43:48'),
(98,56,1,'2019-08-01 15:43:48','2019-08-01 15:43:48'),
(99,56,6,'2019-08-01 15:44:16','2019-08-01 15:44:16'),
(100,56,1,'2019-08-01 15:44:16','2019-08-01 15:44:16'),
(101,56,6,'2019-08-01 15:45:07','2019-08-01 15:45:07'),
(102,56,1,'2019-08-01 15:45:07','2019-08-01 15:45:07'),
(103,56,6,'2019-08-01 15:48:46','2019-08-01 15:48:46'),
(104,56,1,'2019-08-01 15:48:46','2019-08-01 15:48:46'),
(105,55,6,'2019-08-01 16:06:22','2019-08-01 16:06:22'),
(106,55,4,'2019-08-01 16:06:22','2019-08-01 16:06:22'),
(107,58,6,'2019-08-01 16:10:25','2019-08-01 16:10:25'),
(108,59,26,'2019-08-05 13:06:20','2019-08-05 13:06:20'),
(109,56,3,'2019-08-07 15:35:29','2019-08-07 15:35:29'),
(110,56,6,'2019-08-07 15:35:29','2019-08-07 15:35:29'),
(111,57,3,'2019-08-07 15:39:56','2019-08-07 15:39:56'),
(112,57,6,'2019-08-07 15:39:56','2019-08-07 15:39:56'),
(113,58,6,'2019-08-07 15:45:07','2019-08-07 15:45:07'),
(114,59,6,'2019-08-07 15:51:30','2019-08-07 15:51:30'),
(115,60,6,'2019-08-07 15:55:10','2019-08-07 15:55:10'),
(116,61,6,'2019-08-07 15:57:35','2019-08-07 15:57:35'),
(117,62,6,'2019-08-07 16:00:12','2019-08-07 16:00:12'),
(118,63,6,'2019-08-07 16:01:10','2019-08-07 16:01:10'),
(119,64,6,'2019-08-07 16:06:04','2019-08-07 16:06:04'),
(120,65,6,'2019-08-07 16:06:36','2019-08-07 16:06:36'),
(121,66,6,'2019-08-07 16:08:45','2019-08-07 16:08:45'),
(122,67,3,'2019-08-11 02:49:14','2019-08-11 02:49:14'),
(123,67,6,'2019-08-11 02:49:14','2019-08-11 02:49:14'),
(124,67,6,'2019-08-11 02:50:01','2019-08-11 02:50:01'),
(125,67,3,'2019-08-11 02:50:01','2019-08-11 02:50:01'),
(126,67,6,'2019-08-16 15:58:03','2019-08-16 15:58:03'),
(127,67,3,'2019-08-16 15:58:03','2019-08-16 15:58:03'),
(128,67,6,'2019-08-16 15:58:25','2019-08-16 15:58:25'),
(129,67,3,'2019-08-16 15:58:25','2019-08-16 15:58:25'),
(130,67,6,'2019-08-16 15:59:25','2019-08-16 15:59:25'),
(131,67,3,'2019-08-16 15:59:25','2019-08-16 15:59:25'),
(132,67,6,'2019-08-16 16:02:00','2019-08-16 16:02:00'),
(133,67,3,'2019-08-16 16:02:00','2019-08-16 16:02:00'),
(134,59,30,'2019-08-18 04:12:53','2019-08-18 04:12:53'),
(135,59,32,'2019-08-18 04:12:53','2019-08-18 04:12:53'),
(136,60,32,'2019-08-18 04:15:34','2019-08-18 04:15:34'),
(137,61,32,'2019-08-18 04:18:43','2019-08-18 04:18:43'),
(138,62,32,'2019-08-18 04:20:42','2019-08-18 04:20:42'),
(139,63,6,'2019-08-18 04:22:21','2019-08-18 04:22:21'),
(140,64,6,'2019-08-18 04:25:15','2019-08-18 04:25:15');

/*Table structure for table `tbl_project` */

DROP TABLE IF EXISTS `tbl_project`;

CREATE TABLE `tbl_project` (
  `id_project` int(10) NOT NULL AUTO_INCREMENT,
  `nama_project` char(100) DEFAULT NULL,
  `perusahaan` varchar(30) DEFAULT NULL,
  `target_selesai` varchar(30) DEFAULT NULL,
  `nama_qa` varchar(30) DEFAULT NULL,
  `keterangan` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_project`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_project` */

insert  into `tbl_project`(`id_project`,`nama_project`,`perusahaan`,`target_selesai`,`nama_qa`,`keterangan`,`created_at`,`updated_at`) values 
(58,'Aplikasi Data Pegawai','Sagayaku','30/11/2019','Sam Ar','Membuat siste informasi123','2019-08-11 02:49:14','2019-08-16 15:58:03'),
(64,'Program Akutansi','Sagayaku','30/11/2019','Samsul Ar Qa','asdasd','2019-08-18 04:25:15','2019-08-18 04:25:15');

/*Table structure for table `tbl_task` */

DROP TABLE IF EXISTS `tbl_task`;

CREATE TABLE `tbl_task` (
  `id_task` int(10) NOT NULL AUTO_INCREMENT,
  `id_project` int(10) DEFAULT NULL,
  `id_users` int(10) DEFAULT NULL,
  `nama_task` varchar(30) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `keterangan` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_task`),
  KEY `id_project` (`id_project`),
  KEY `id_task` (`id_task`),
  KEY `id_users` (`id_users`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_task` */

insert  into `tbl_task`(`id_task`,`id_project`,`id_users`,`nama_task`,`status`,`keterangan`,`created_at`,`updated_at`) values 
(1,64,4,'Membuat Login','Issue',NULL,'2019-08-18 11:26:33','2019-08-18 11:54:54');

/*Table structure for table `tbl_testing` */

DROP TABLE IF EXISTS `tbl_testing`;

CREATE TABLE `tbl_testing` (
  `id_testing` int(10) NOT NULL AUTO_INCREMENT,
  `id_project` int(10) DEFAULT NULL,
  `id_ussers` int(10) DEFAULT NULL,
  `id_task` int(10) DEFAULT NULL,
  `nama_test` varchar(30) DEFAULT NULL,
  `jenis_test` varchar(20) DEFAULT NULL,
  `status_testing` varchar(20) DEFAULT NULL,
  `keterangan_test` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_testing`),
  KEY `id_task` (`id_task`),
  CONSTRAINT `tbl_testing_ibfk_1` FOREIGN KEY (`id_task`) REFERENCES `tbl_task` (`id_task`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_testing` */

insert  into `tbl_testing`(`id_testing`,`id_project`,`id_ussers`,`id_task`,`nama_test`,`jenis_test`,`status_testing`,`keterangan_test`,`created_at`,`updated_at`) values 
(41,64,3,1,'Membuat Login','Negative Case','Solved','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>ads</p>\r\n</body>\r\n</html>','2019-08-18 12:18:40','2019-08-18 05:18:40'),
(42,64,6,1,'Membuat Login','Positive Case','Issue','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>asd</p>\r\n</body>\r\n</html>','2019-08-18 11:54:54','2019-08-18 11:54:54');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id_users` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `no_tlp` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_users`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id_users`,`name`,`no_tlp`,`email`,`email_verified_at`,`password`,`role`,`remember_token`,`created_at`,`updated_at`) values 
(1,'Samsul Arifin','08986969882','samsul026@gmail.com',NULL,'$2y$10$Vgi.GEclqh0Zj1RhUDBFs.2cQN4G7IOH4SfYCT.mniQg91zRUZ.Jm','PM','DvHIV6TbgA7jKmr0eamN5dFSwnZyP3kC9bJ5uk89pXE2NVavbFTQglgfMWsR','2019-03-06 07:01:10','2019-03-06 07:01:10'),
(3,'Hadi','082121765707','sarah@gmail.com',NULL,'$2y$10$YihDpo1GoXn.CzqzwUANsO8twtwNzxsZerXTMGECbgaAkaOk2D2xG','Programer','h7Vzhze6ZciHlS332f31gePtUvCnWyBflAMnk1ylf7z2UnaC3YuA12MbIFKu','2019-05-13 03:19:38','2019-05-13 03:19:38'),
(4,'Samsul Ar Qa','082121765707\r\n','160613058@gmail.com',NULL,'$2y$10$t/gCLwezeskTdnKMh2ubhOsGKA1RUgd0Qiol7YrzgbBsfMRAs/7Uu','QA','WS3R7rb5XDsDpDe5ZebNnbNc6FRqUurRdkXWMvkMmyxtTN2WTKPKyffdnl5K','2019-05-24 13:21:37','2019-05-24 13:21:37'),
(6,'Anggi SOnia','082121765707\r\n','anggi_sonia@yahoo.com',NULL,'$2y$10$4QlrQchSyVVQw4/FU4uNRODJn8H2VUf566G7xmZVjMYVYdK7fQAdy','Programer','CBNA7RtPlBgzsRRz5rBvfLagEd71hc8C2g9XuXUBEW7RKsmq0Cue1l0MHfp2','2019-05-27 00:34:26','2019-05-27 00:34:26'),
(7,'Dandi','082121765707\r\n','sagayaku23@gmail.com',NULL,'$2y$10$t6G8iLF99AYTk42DTdpy/uIpMTmxHw4lAZc/1vRa6.obGf.yWm5Y6','Programer','wssNFB2gsNTIxfpGz8mSmbbR2hLYXHXccepLsBGZ9eYy8E9rJ4QpBosQAY9C','2019-07-25 06:51:03','2019-07-25 06:51:03'),
(30,'Rizki','082121765707\r\n','R21@gmail.com',NULL,'$2y$10$SaFy/igr6XrsvtGJHYpa7OgTQJil4i3/LDduRwPxrq1WJpxlD8wuC','Programer',NULL,'2019-08-07 14:40:26','2019-08-07 14:40:26'),
(32,'Samsul Arifin Programer','08986969882','sss@gmail.com',NULL,'$2y$10$X.o1HCVlWQgBWTfpv2gsMO8uVnMM8s2cyuSREbmz4/MUAfhRR3Tlu','Programer',NULL,'2019-08-10 01:57:43','2019-08-10 01:57:43');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
